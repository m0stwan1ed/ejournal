﻿namespace ejournal
{
    partial class Form_Manage_Subjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button_Add_Subject = new System.Windows.Forms.Button();
            this.button_Remove_Subject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 41);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(262, 238);
            this.listBox1.TabIndex = 0;
            // 
            // button_Add_Subject
            // 
            this.button_Add_Subject.Location = new System.Drawing.Point(12, 12);
            this.button_Add_Subject.Name = "button_Add_Subject";
            this.button_Add_Subject.Size = new System.Drawing.Size(128, 23);
            this.button_Add_Subject.TabIndex = 1;
            this.button_Add_Subject.Text = "button_Add_Subject";
            this.button_Add_Subject.UseVisualStyleBackColor = true;
            this.button_Add_Subject.Click += new System.EventHandler(this.button_Add_Subject_Click);
            // 
            // button_Remove_Subject
            // 
            this.button_Remove_Subject.Location = new System.Drawing.Point(146, 12);
            this.button_Remove_Subject.Name = "button_Remove_Subject";
            this.button_Remove_Subject.Size = new System.Drawing.Size(128, 23);
            this.button_Remove_Subject.TabIndex = 2;
            this.button_Remove_Subject.Text = "button_Remove_Subject";
            this.button_Remove_Subject.UseVisualStyleBackColor = true;
            this.button_Remove_Subject.Click += new System.EventHandler(this.button_Remove_Subject_Click);
            // 
            // Form_Manage_Subjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 296);
            this.Controls.Add(this.button_Remove_Subject);
            this.Controls.Add(this.button_Add_Subject);
            this.Controls.Add(this.listBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Manage_Subjects";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_Manage_Subjects";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button_Add_Subject;
        private System.Windows.Forms.Button button_Remove_Subject;
    }
}