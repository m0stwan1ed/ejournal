﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;


namespace ejournal
{
    public partial class Form_ShowStudentsData : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных
        
        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        string language = ""; // Язык приложения

        int studentsId; // Идентификатор студента в базе данных

        public Form_ShowStudentsData(int studentsId, SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор окна
        {
            InitializeComponent();

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;
            this.studentsId = studentsId;
            label_studentsID_value.Text = studentsId.ToString();

            // Запрос данных о студенте к базе данных и выведение их на экран
            try
            { 
                string sqlQuery = "SELECT studentsSurname, studentsName, studentsPatronym, studentsPhoneNumber, studentsEmail, groupName, " +
                               "parentsFirstSurname, parentsFirstName, parentsFirstPatronym, parentsFirstPhoneNumber, parentsFirstEmail, " +
                               "parentsSecondSurname, parentsSecondName, parentsSecondPatronym, parentsSecondPhoneNumber, parentsSecondEmail " +
                               "FROM students WHERE studentsID=" + studentsId;
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        label_students_surname_value.Text = answerTable.Rows[0].ItemArray[0].ToString();
                        label_students_name_value.Text = answerTable.Rows[0].ItemArray[1].ToString();
                        label_students_patronym_value.Text = answerTable.Rows[0].ItemArray[2].ToString();
                        label_students_phone_number_value.Text = answerTable.Rows[0].ItemArray[3].ToString();
                        label_students_email_value.Text = answerTable.Rows[0].ItemArray[4].ToString();
                        label_students_group_value.Text = answerTable.Rows[0].ItemArray[5].ToString();

                        label_parents_first_surname_value.Text = answerTable.Rows[0].ItemArray[6].ToString();
                        label_parents_first_name_value.Text = answerTable.Rows[0].ItemArray[7].ToString();
                        label_parents_first_patronym_value.Text = answerTable.Rows[0].ItemArray[8].ToString();
                        label_parents_first_phone_number_value.Text = answerTable.Rows[0].ItemArray[9].ToString();
                        label_parents_first_email_value.Text = answerTable.Rows[0].ItemArray[10].ToString();

                        label_parents_second_surname_value.Text = answerTable.Rows[0].ItemArray[11].ToString();
                        label_parents_second_name_value.Text = answerTable.Rows[0].ItemArray[12].ToString();
                        label_parents_second_patronym_value.Text = answerTable.Rows[0].ItemArray[13].ToString();
                        label_parents_second_phone_number_value.Text = answerTable.Rows[0].ItemArray[14].ToString();
                        label_parents_second_email_value.Text = answerTable.Rows[0].ItemArray[15].ToString();
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        void updateLocalization()
        {
            this.Text = localization.Element("localization").Element("studentsData").Element("studentsData").Value;

            label_studentsID.Text = localization.Element("localization").Element("studentsData").Element("studentsID").Value + " :";

            groupBox1.Text = localization.Element("localization").Element("studentsData").Element("student").Value;
            label_students_surname.Text = localization.Element("localization").Element("studentsData").Element("surname").Value + " :";
            label_students_name.Text = localization.Element("localization").Element("studentsData").Element("name").Value + " :";
            label_students_patronym.Text = localization.Element("localization").Element("studentsData").Element("patronym").Value + " :";
            label_students_phone_number.Text = localization.Element("localization").Element("studentsData").Element("phoneNumber").Value + " :";
            label_students_email.Text = localization.Element("localization").Element("studentsData").Element("eMail").Value + " :";
            label_students_group.Text = localization.Element("localization").Element("studentsData").Element("group").Value + " :";

            groupBox2.Text = localization.Element("localization").Element("studentsData").Element("parents").Value;
            label_parents_first_surname.Text = localization.Element("localization").Element("studentsData").Element("surname").Value + " :";
            label_parents_first_name.Text = localization.Element("localization").Element("studentsData").Element("name").Value + " :";
            label_parents_first_patronym.Text = localization.Element("localization").Element("studentsData").Element("patronym").Value + " :";
            label_parents_first_phone_number.Text = localization.Element("localization").Element("studentsData").Element("phoneNumber").Value + " :";
            label_parents_first_email.Text = localization.Element("localization").Element("studentsData").Element("eMail").Value + " :";

            label_parents_second_surname.Text = localization.Element("localization").Element("studentsData").Element("surname").Value + " :";
            label_parents_second_name.Text = localization.Element("localization").Element("studentsData").Element("name").Value + " :";
            label_parents_second_patronym.Text = localization.Element("localization").Element("studentsData").Element("patronym").Value + " :";
            label_parents_second_phone_number.Text = localization.Element("localization").Element("studentsData").Element("phoneNumber").Value + " :";
            label_parents_second_email.Text = localization.Element("localization").Element("studentsData").Element("eMail").Value + " :";

            button_edit.Text = localization.Element("localization").Element("studentsData").Element("edit").Value;
        } // Метод обновления локализации окна

        private void button_edit_Click(object sender, EventArgs e) // Нажатие на кнопку "Редактировать данные"
        {
            Form_Add_Student formCorrectStudent = new Form_Add_Student(studentsId, databaseConnection, sqlCommand);
            formCorrectStudent.ShowDialog();
            formCorrectStudent.Dispose();

            // Обновление данных о студенте
            try
            {
                string sqlQuery = "SELECT studentsSurname, studentsName, studentsPatronym, studentsPhoneNumber, studentsEmail, groupName, " +
                               "parentsFirstSurname, parentsFirstName, parentsFirstPatronym, parentsFirstPhoneNumber, parentsFirstEmail, " +
                               "parentsSecondSurname, parentsSecondName, parentsSecondPatronym, parentsSecondPhoneNumber, parentsSecondEmail " +
                               "FROM students WHERE studentsID=" + studentsId;
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        label_students_surname_value.Text = answerTable.Rows[0].ItemArray[0].ToString();
                        label_students_name_value.Text = answerTable.Rows[0].ItemArray[1].ToString();
                        label_students_patronym_value.Text = answerTable.Rows[0].ItemArray[2].ToString();
                        label_students_phone_number_value.Text = answerTable.Rows[0].ItemArray[3].ToString();
                        label_students_email_value.Text = answerTable.Rows[0].ItemArray[4].ToString();
                        label_students_group_value.Text = answerTable.Rows[0].ItemArray[5].ToString();

                        label_parents_first_surname_value.Text = answerTable.Rows[0].ItemArray[6].ToString();
                        label_parents_first_name_value.Text = answerTable.Rows[0].ItemArray[7].ToString();
                        label_parents_first_patronym_value.Text = answerTable.Rows[0].ItemArray[8].ToString();
                        label_parents_first_phone_number_value.Text = answerTable.Rows[0].ItemArray[9].ToString();
                        label_parents_first_email_value.Text = answerTable.Rows[0].ItemArray[10].ToString();

                        label_parents_second_surname_value.Text = answerTable.Rows[0].ItemArray[11].ToString();
                        label_parents_second_name_value.Text = answerTable.Rows[0].ItemArray[12].ToString();
                        label_parents_second_patronym_value.Text = answerTable.Rows[0].ItemArray[13].ToString();
                        label_parents_second_phone_number_value.Text = answerTable.Rows[0].ItemArray[14].ToString();
                        label_parents_second_email_value.Text = answerTable.Rows[0].ItemArray[15].ToString();
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
