﻿namespace ejournal
{
    partial class Form_Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_average_marks = new System.Windows.Forms.Button();
            this.button_list_marks = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_average_marks
            // 
            this.button_average_marks.Image = global::ejournal.Properties.Resources.ejournal_report_avg;
            this.button_average_marks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_average_marks.Location = new System.Drawing.Point(12, 73);
            this.button_average_marks.Name = "button_average_marks";
            this.button_average_marks.Size = new System.Drawing.Size(145, 55);
            this.button_average_marks.TabIndex = 1;
            this.button_average_marks.Text = "Average marks";
            this.button_average_marks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_average_marks.UseVisualStyleBackColor = true;
            this.button_average_marks.Click += new System.EventHandler(this.button_average_marks_Click);
            // 
            // button_list_marks
            // 
            this.button_list_marks.Image = global::ejournal.Properties.Resources.ejournal_report_list;
            this.button_list_marks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_list_marks.Location = new System.Drawing.Point(12, 12);
            this.button_list_marks.Name = "button_list_marks";
            this.button_list_marks.Size = new System.Drawing.Size(145, 55);
            this.button_list_marks.TabIndex = 0;
            this.button_list_marks.Text = "List marks";
            this.button_list_marks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_list_marks.UseVisualStyleBackColor = true;
            this.button_list_marks.Click += new System.EventHandler(this.button_list_marks_Click);
            // 
            // Form_Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(169, 141);
            this.Controls.Add(this.button_average_marks);
            this.Controls.Add(this.button_list_marks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Reports";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_Reports";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_list_marks;
        private System.Windows.Forms.Button button_average_marks;
    }
}