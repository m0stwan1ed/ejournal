﻿namespace ejournal
{
    partial class Form_Manage_Students
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_add_group = new System.Windows.Forms.Button();
            this.button_remove_group = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button_add_student = new System.Windows.Forms.Button();
            this.button_remove_student = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(284, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.Text = "[Choose group]";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button_add_group
            // 
            this.button_add_group.Location = new System.Drawing.Point(189, 39);
            this.button_add_group.Name = "button_add_group";
            this.button_add_group.Size = new System.Drawing.Size(107, 23);
            this.button_add_group.TabIndex = 2;
            this.button_add_group.Text = "Add group";
            this.button_add_group.UseVisualStyleBackColor = true;
            this.button_add_group.Click += new System.EventHandler(this.button_add_group_Click);
            // 
            // button_remove_group
            // 
            this.button_remove_group.Location = new System.Drawing.Point(189, 68);
            this.button_remove_group.Name = "button_remove_group";
            this.button_remove_group.Size = new System.Drawing.Size(107, 23);
            this.button_remove_group.TabIndex = 3;
            this.button_remove_group.Text = "Remove group";
            this.button_remove_group.UseVisualStyleBackColor = true;
            this.button_remove_group.Click += new System.EventHandler(this.button_remove_group_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 97);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(284, 342);
            this.listBox1.TabIndex = 0;
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // button_add_student
            // 
            this.button_add_student.Location = new System.Drawing.Point(12, 445);
            this.button_add_student.Name = "button_add_student";
            this.button_add_student.Size = new System.Drawing.Size(139, 23);
            this.button_add_student.TabIndex = 3;
            this.button_add_student.Text = "Add student";
            this.button_add_student.UseVisualStyleBackColor = true;
            this.button_add_student.Click += new System.EventHandler(this.button_add_student_Click);
            // 
            // button_remove_student
            // 
            this.button_remove_student.Location = new System.Drawing.Point(157, 445);
            this.button_remove_student.Name = "button_remove_student";
            this.button_remove_student.Size = new System.Drawing.Size(139, 23);
            this.button_remove_student.TabIndex = 4;
            this.button_remove_student.Text = "Remove student";
            this.button_remove_student.UseVisualStyleBackColor = true;
            this.button_remove_student.Click += new System.EventHandler(this.button_remove_student_Click);
            // 
            // Form_Manage_Students
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 478);
            this.Controls.Add(this.button_remove_student);
            this.Controls.Add(this.button_add_student);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button_remove_group);
            this.Controls.Add(this.button_add_group);
            this.Controls.Add(this.comboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form_Manage_Students";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mange students";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button_add_group;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_remove_group;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button_add_student;
        private System.Windows.Forms.Button button_remove_student;
    }
}