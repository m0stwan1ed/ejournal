﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Manage_Data : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        public Form_Manage_Data(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор окна
        {
            InitializeComponent();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            string language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();
        }

        void updateLocalization() // Метод обновления локализации окна
        {
            this.Text = localization.Element("localization").Element("manageData").Element("manageData").Value;
            button_manage_subjects.Text = localization.Element("localization").Element("manageData").Element("manageSubjects").Value;
            button_manage_students.Text = localization.Element("localization").Element("manageData").Element("manageStudents").Value;
        }

        private void button_manage_students_Click(object sender, EventArgs e) // Нажатие на кнопку "Управление студентами"
        {
            Form formMangeStudents = new Form_Manage_Students(databaseConnection, sqlCommand); // Инициализация окна
            formMangeStudents.ShowDialog(); // Отображение окна в виде диалогового
            formMangeStudents.Dispose(); // Уничтожение окна после его закрытия
        }

        private void button_manage_subjects_Click(object sender, EventArgs e) // Нажатие на кнопку "Управление группами"
        {
            Form_Manage_Subjects formManageSubjects = new Form_Manage_Subjects(databaseConnection, sqlCommand);
            formManageSubjects.ShowDialog();
            formManageSubjects.Dispose();
        }
    }
}
