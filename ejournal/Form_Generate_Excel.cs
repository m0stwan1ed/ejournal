﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

// Библиотека для работы с документами Excel
using Excel = Microsoft.Office.Interop.Excel;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Generate_Excel : Form
    {
        SQLiteConnection databaseConnection;
        SQLiteCommand sqlCommand;
        string sqlQuery;
        DataTable answerTable;

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\";
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml";

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        string language = "";
        string marks = "";
        string group = "";
        string subject = "";
        string activity = "";
        string date = "";
        string quantity = "";
        string name = "";
        string mark = "";

        public Form_Generate_Excel(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();

            if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal"))
            {
                Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\");
            }

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            // Считывание списка групп
            try
            {
                sqlQuery = "SELECT groupName FROM groups ORDER BY groupName";
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox1.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }

                sqlQuery = "SELECT subjectName FROM subjects ORDER BY subjectName";
                answerTable = new DataTable();
                adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox2.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();
        }

        public void updateLocalization()
        {
            this.Text = localization.Element("localization").Element("generateExcel").Element("generateExcel").Value;
            label_group.Text = localization.Element("localization").Element("generateExcel").Element("group").Value + " :";
            group = localization.Element("localization").Element("generateExcel").Element("group").Value;
            comboBox1.Text = "[" + localization.Element("localization").Element("generateExcel").Element("chooseGroup").Value + "]";
            label_Subject.Text = localization.Element("localization").Element("generateExcel").Element("subject").Value + " :";
            subject = localization.Element("localization").Element("generateExcel").Element("subject").Value;
            comboBox2.Text = "[" + localization.Element("localization").Element("generateExcel").Element("chooseSubject").Value + "]";
            label_activity.Text = localization.Element("localization").Element("generateExcel").Element("activity").Value + " :";
            activity = localization.Element("localization").Element("generateExcel").Element("activity").Value;
            label_date.Text = localization.Element("localization").Element("generateExcel").Element("date").Value + " :";
            date = localization.Element("localization").Element("generateExcel").Element("date").Value;
            button_generate.Text = localization.Element("localization").Element("generateExcel").Element("generate").Value;

            marks = localization.Element("localization").Element("generateExcel").Element("marks").Value;
            quantity = localization.Element("localization").Element("generateExcel").Element("quantity").Value;
            name = localization.Element("localization").Element("generateExcel").Element("name").Value;
            mark = localization.Element("localization").Element("generateExcel").Element("mark").Value;
        }

        private void button_generate_Click(object sender, EventArgs e) // Нажатие на кнопку "Создать документ"
        {
            if((comboBox1.SelectedItem != null)&&(comboBox2.SelectedItem != null)&&(!textBox_activity.Text.Equals("")))
            {
                answerTable = new DataTable();

                sqlQuery = "SELECT studentsSurname || ' ' || studentsName || ' ' || studentsPatronym " +
                           "FROM students WHERE groupName='" + comboBox1.SelectedItem.ToString() + "'" +
                           "ORDER BY studentsSurname, studentsName, studentsPatronym";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);


                Excel.Application excelApp = new Excel.Application(); // Инициализация приложения Excel
                excelApp.Workbooks.Add(); // Добавление листа
                excelApp.Worksheets[1].Name = marks; // Изменение названия листа

                // Указание в листе стандартных полей и их данных - Группа, Название активности, Дата, Кол-во студентов
                excelApp.Worksheets[1].Range["A1"].Value = group;
                excelApp.Worksheets[1].Range["A2"].Value = comboBox1.SelectedItem.ToString();
                excelApp.Worksheets[1].Range["B1"].Value = subject;
                excelApp.Worksheets[1].Range["B2"].Value = comboBox2.SelectedItem.ToString();
                excelApp.Worksheets[1].Range["C1"].Value = activity;
                excelApp.Worksheets[1].Range["C2"].Value = textBox_activity.Text;
                excelApp.Worksheets[1].Range["D1"].Value = date;
                excelApp.Worksheets[1].Range["D2"].Value = dateTimePicker1.Value.Date.ToString("dd.MM.yyyy");
                excelApp.Worksheets[1].Range["E1"].Value = quantity;
                excelApp.Worksheets[1].Range["E2"].Value = answerTable.Rows.Count;

                // Форматирование ячеек
                excelApp.Worksheets[1].Range["A1:E2"].Columns.Autofit();
                excelApp.Worksheets[1].Range["A1:E2"].Cells.Borders.LineStyle = 1;

                // Запрет изменения сервисных ячеек
                excelApp.Worksheets[1].Cells.Locked = false;
                excelApp.Worksheets[1].Range["A1:E2"].Locked = true;
                excelApp.Worksheets[1].Protect("ejournalPassword", false, true, true, true, true, true, true, false, false, false, false, false, false, false, false);

                // Указание столбцов с именами студентов и их оценками
                excelApp.Worksheets[1].Range["A3"].Value = name;
                excelApp.Worksheets[1].Range["B3"].Value = mark;

                int end = 0;
                // Добавление студентов из списка группы в лист Excel
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    excelApp.Worksheets[1].Range["A" + (i + 4)].Value = answerTable.Rows[i].ItemArray[0];
                    end = i + 4;
                }

                // Форматирование ячеек
                excelApp.Worksheets[1].Range["A1:B" + end].Columns.AutoFit();
                excelApp.Worksheets[1].Range["B3:B" + end].Cells.Borders.LineStyle = 3;
                excelApp.Worksheets[1].Range["A1:A" + end].Cells.Borders.LineStyle = 1;
                excelApp.Worksheets[1].Range["A3:A" + end].Locked = true;

                excelApp.Visible = true; // Вывод приложения Excel на экран
            }
        }
    }
}