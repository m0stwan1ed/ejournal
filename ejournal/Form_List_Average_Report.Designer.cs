﻿namespace ejournal
{
    partial class Form_List_Average_Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button_get_report = new System.Windows.Forms.Button();
            this.comboBox_type = new System.Windows.Forms.ComboBox();
            this.comboBox_group = new System.Windows.Forms.ComboBox();
            this.comboBox_student = new System.Windows.Forms.ComboBox();
            this.comboBox_subject = new System.Windows.Forms.ComboBox();
            this.comboBox_activity = new System.Windows.Forms.ComboBox();
            this.checkBox_date = new System.Windows.Forms.CheckBox();
            this.dateTimePicker_to = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_from = new System.Windows.Forms.DateTimePicker();
            this.label_date_to = new System.Windows.Forms.Label();
            this.label_date_from = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_date);
            this.groupBox1.Controls.Add(this.dateTimePicker_to);
            this.groupBox1.Controls.Add(this.dateTimePicker_from);
            this.groupBox1.Controls.Add(this.label_date_to);
            this.groupBox1.Controls.Add(this.label_date_from);
            this.groupBox1.Controls.Add(this.comboBox_activity);
            this.groupBox1.Controls.Add(this.comboBox_subject);
            this.groupBox1.Controls.Add(this.comboBox_student);
            this.groupBox1.Controls.Add(this.comboBox_group);
            this.groupBox1.Controls.Add(this.comboBox_type);
            this.groupBox1.Controls.Add(this.button_get_report);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(790, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameters";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 129);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(790, 444);
            this.dataGridView1.TabIndex = 1;
            // 
            // button_get_report
            // 
            this.button_get_report.Location = new System.Drawing.Point(639, 27);
            this.button_get_report.Name = "button_get_report";
            this.button_get_report.Size = new System.Drawing.Size(145, 55);
            this.button_get_report.TabIndex = 0;
            this.button_get_report.Text = "Get report";
            this.button_get_report.UseVisualStyleBackColor = true;
            this.button_get_report.Click += new System.EventHandler(this.button_get_report_Click);
            // 
            // comboBox_type
            // 
            this.comboBox_type.FormattingEnabled = true;
            this.comboBox_type.Location = new System.Drawing.Point(6, 19);
            this.comboBox_type.Name = "comboBox_type";
            this.comboBox_type.Size = new System.Drawing.Size(180, 21);
            this.comboBox_type.TabIndex = 1;
            this.comboBox_type.Text = "[Choose type]";
            this.comboBox_type.SelectedIndexChanged += new System.EventHandler(this.comboBox_type_SelectedIndexChanged);
            // 
            // comboBox_group
            // 
            this.comboBox_group.Enabled = false;
            this.comboBox_group.FormattingEnabled = true;
            this.comboBox_group.Location = new System.Drawing.Point(6, 46);
            this.comboBox_group.Name = "comboBox_group";
            this.comboBox_group.Size = new System.Drawing.Size(180, 21);
            this.comboBox_group.TabIndex = 8;
            this.comboBox_group.Text = "[Choose group]";
            this.comboBox_group.SelectedIndexChanged += new System.EventHandler(this.comboBox_group_SelectedIndexChanged);
            // 
            // comboBox_student
            // 
            this.comboBox_student.Enabled = false;
            this.comboBox_student.FormattingEnabled = true;
            this.comboBox_student.Location = new System.Drawing.Point(6, 73);
            this.comboBox_student.Name = "comboBox_student";
            this.comboBox_student.Size = new System.Drawing.Size(180, 21);
            this.comboBox_student.TabIndex = 10;
            this.comboBox_student.Text = "[Choose student]";
            // 
            // comboBox_subject
            // 
            this.comboBox_subject.Enabled = false;
            this.comboBox_subject.FormattingEnabled = true;
            this.comboBox_subject.Location = new System.Drawing.Point(192, 45);
            this.comboBox_subject.Name = "comboBox_subject";
            this.comboBox_subject.Size = new System.Drawing.Size(180, 21);
            this.comboBox_subject.TabIndex = 12;
            this.comboBox_subject.Text = "[Choose subject]";
            this.comboBox_subject.SelectedIndexChanged += new System.EventHandler(this.comboBox_subject_SelectedIndexChanged);
            // 
            // comboBox_activity
            // 
            this.comboBox_activity.Enabled = false;
            this.comboBox_activity.FormattingEnabled = true;
            this.comboBox_activity.Location = new System.Drawing.Point(192, 73);
            this.comboBox_activity.Name = "comboBox_activity";
            this.comboBox_activity.Size = new System.Drawing.Size(180, 21);
            this.comboBox_activity.TabIndex = 14;
            this.comboBox_activity.Text = "[Choose activity]";
            // 
            // checkBox_date
            // 
            this.checkBox_date.AutoSize = true;
            this.checkBox_date.Location = new System.Drawing.Point(381, 19);
            this.checkBox_date.Name = "checkBox_date";
            this.checkBox_date.Size = new System.Drawing.Size(49, 17);
            this.checkBox_date.TabIndex = 19;
            this.checkBox_date.Text = "Date";
            this.checkBox_date.UseVisualStyleBackColor = true;
            this.checkBox_date.CheckedChanged += new System.EventHandler(this.checkBox_date_CheckedChanged);
            // 
            // dateTimePicker_to
            // 
            this.dateTimePicker_to.Enabled = false;
            this.dateTimePicker_to.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_to.Location = new System.Drawing.Point(421, 68);
            this.dateTimePicker_to.Name = "dateTimePicker_to";
            this.dateTimePicker_to.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker_to.TabIndex = 18;
            // 
            // dateTimePicker_from
            // 
            this.dateTimePicker_from.Enabled = false;
            this.dateTimePicker_from.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_from.Location = new System.Drawing.Point(421, 42);
            this.dateTimePicker_from.Name = "dateTimePicker_from";
            this.dateTimePicker_from.Size = new System.Drawing.Size(129, 20);
            this.dateTimePicker_from.TabIndex = 17;
            // 
            // label_date_to
            // 
            this.label_date_to.AutoSize = true;
            this.label_date_to.Enabled = false;
            this.label_date_to.Location = new System.Drawing.Point(378, 74);
            this.label_date_to.Name = "label_date_to";
            this.label_date_to.Size = new System.Drawing.Size(26, 13);
            this.label_date_to.TabIndex = 16;
            this.label_date_to.Text = "To :";
            // 
            // label_date_from
            // 
            this.label_date_from.AutoSize = true;
            this.label_date_from.Enabled = false;
            this.label_date_from.Location = new System.Drawing.Point(378, 48);
            this.label_date_from.Name = "label_date_from";
            this.label_date_from.Size = new System.Drawing.Size(36, 13);
            this.label_date_from.TabIndex = 15;
            this.label_date_from.Text = "From :";
            // 
            // Form_List_Average_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 585);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_List_Average_Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_List_Average_Report";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button_get_report;
        private System.Windows.Forms.ComboBox comboBox_type;
        private System.Windows.Forms.ComboBox comboBox_group;
        private System.Windows.Forms.ComboBox comboBox_student;
        private System.Windows.Forms.ComboBox comboBox_subject;
        private System.Windows.Forms.ComboBox comboBox_activity;
        private System.Windows.Forms.CheckBox checkBox_date;
        private System.Windows.Forms.DateTimePicker dateTimePicker_to;
        private System.Windows.Forms.DateTimePicker dateTimePicker_from;
        private System.Windows.Forms.Label label_date_to;
        private System.Windows.Forms.Label label_date_from;
    }
}