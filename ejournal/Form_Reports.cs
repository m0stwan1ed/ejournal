﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Reports : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        public Form_Reports(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            string language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();
        }

        void updateLocalization() // Метод обновления локализации
        {
            this.Text = localization.Element("localization").Element("reports").Element("reports").Value;
            button_list_marks.Text = localization.Element("localization").Element("reports").Element("listMarks").Value;
            button_average_marks.Text = localization.Element("localization").Element("reports").Element("averageMarks").Value;
        }

        private void button_list_marks_Click(object sender, EventArgs e) // Нажатие на кнопку "Список оценок"
        {
            Form_List_Marks_Report formListMarksReport = new Form_List_Marks_Report(databaseConnection, sqlCommand);
            formListMarksReport.ShowDialog();
            formListMarksReport.Dispose();
        }

        private void button_average_marks_Click(object sender, EventArgs e) // Нажатие на кнопку "Средние оценки"
        {
            Form_List_Average_Report formListAverageReport = new Form_List_Average_Report(databaseConnection, sqlCommand);
            formListAverageReport.ShowDialog();
            formListAverageReport.Dispose();
        }
    }
}
