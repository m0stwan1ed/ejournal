﻿using System;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

namespace ejournal
{
    public partial class Form_Settings : Form
    {
        XDocument settingsXML = new XDocument(); // Файл настроек
        XDocument localization = new XDocument(); // Файл локализации

        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        string language; // Язык приложения

        public Form_Settings() // Конструктор формы
        {
            InitializeComponent();
            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            int languages = comboBox1.Items.Count;
            for(int i = 0; i < languages; i++)
            {
                if (comboBox1.Items[i].ToString().Equals(language)) comboBox1.SelectedIndex = i;
            }
            textBox_username.Text = settingsXML.Element("settings").Element("username").Value;

            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();
        }

        public void updateLocalization() // Метод обновления локализации формы
        {
            this.Text = localization.Element("localization").Element("settings").Element("settings").Value;
            label_language.Text = localization.Element("localization").Element("settings").Element("language").Value;
            button_Save.Text = localization.Element("localization").Element("settings").Element("save").Value;
            label_username.Text = localization.Element("localization").Element("settings").Element("username").Value;
        }

        private void button_Save_Click(object sender, EventArgs e) // Нажатие ни кнопку "Сохраниь настройки"
        {
            settingsXML.Element("settings").Element("language").Value = comboBox1.SelectedItem.ToString();
            settingsXML.Element("settings").Element("username").Value = textBox_username.Text;
            settingsXML.Save(settingsXMLPath);
            this.Close();
        }
    }
}
