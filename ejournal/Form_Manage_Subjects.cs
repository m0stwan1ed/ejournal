﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Manage_Subjects : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML
        string language = "";

        string removeSubjectConfirmation = "";
        string removeSubject = "";

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        public Form_Manage_Subjects(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();
            reloadListBox(); // Обновление списка предметов
        }

        void updateLocalization() // Метод обновления локализации формы
        {
            this.Text = localization.Element("localization").Element("manageSubjects").Element("manageSubjects").Value;
            button_Add_Subject.Text = localization.Element("localization").Element("manageSubjects").Element("addSubject").Value;
            button_Remove_Subject.Text = localization.Element("localization").Element("manageSubjects").Element("removeSubject").Value;
            removeSubjectConfirmation = localization.Element("localization").Element("manageSubjects").Element("removeSubjectConfirmation").Value;
            removeSubject = localization.Element("localization").Element("manageSubjects").Element("removeSubject").Value;
        }

        void reloadListBox() // Метод, что выполняет обновление списка студентов
        {
            listBox1.SelectedItem = null; // Снятие выбора в листБоксе

            DataTable answerTable = new DataTable();
            string sqlQuery = "SELECT subjectName FROM subjects ORDER BY subjectName";
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            listBox1.Items.Clear(); // Очистка листБокса
            if (answerTable.Rows.Count > 0)
            {
                // Добавление студентов из списка в листБокс
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    listBox1.Items.Add(answerTable.Rows[i].ItemArray[0]);
                }
            }
        }

        private void button_Add_Subject_Click(object sender, EventArgs e) // Нажатие на кнопку "Добавить предмет"
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Input name of subject", "New subject", "", -1, -1); // Вызов инпутБокса из VisualBasic и получение ввода пользователя
            if (input.Length != 0) // Если длинна ввода не 0
            {
                sqlCommand.CommandText = "INSERT INTO subjects (subjectName) VALUES ('" + input + "')";
                sqlCommand.ExecuteNonQuery();
                reloadListBox();
            }
        }

        private void button_Remove_Subject_Click(object sender, EventArgs e) // Нажатие на кнопку "Удалить предмет"
        {
            if(listBox1.SelectedItem != null)
            {
                DialogResult dialogRequest = MessageBox.Show(removeSubjectConfirmation + " '" + listBox1.SelectedItem.ToString() + "'?", removeSubject, MessageBoxButtons.YesNo); // Вывод на экран диалогового окна с фопросом об удалении группы
                try
                {
                    if (dialogRequest == DialogResult.Yes) // Если результат диалога "Да"
                    {
                        sqlCommand.CommandText = "DELETE FROM subjects WHERE subjectName='" + listBox1.SelectedItem.ToString() + "'";
                        sqlCommand.ExecuteNonQuery();
                        reloadListBox();
                    }
                }
                catch (System.Data.SQLite.SQLiteException)
                {
                    MessageBox.Show("Can't remove subject: used in database");
                }
            }
        }
    }
}
