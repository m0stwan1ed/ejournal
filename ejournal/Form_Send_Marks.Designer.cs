﻿namespace ejournal
{
    partial class Form_Send_Marks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_Information = new System.Windows.Forms.GroupBox();
            this.label_Subject = new System.Windows.Forms.Label();
            this.label_Students = new System.Windows.Forms.Label();
            this.label_Activity = new System.Windows.Forms.Label();
            this.label_Group = new System.Windows.Forms.Label();
            this.label_Date = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_Parameters = new System.Windows.Forms.GroupBox();
            this.checkBoxUploadMarks = new System.Windows.Forms.CheckBox();
            this.buttonSendMarks = new System.Windows.Forms.Button();
            this.checkBoxSendToParentsSecondEmail = new System.Windows.Forms.CheckBox();
            this.checkBoxSendToParentsFirstEmail = new System.Windows.Forms.CheckBox();
            this.checkBoxSendToStudents = new System.Windows.Forms.CheckBox();
            this.checkBoxSendReport = new System.Windows.Forms.CheckBox();
            this.groupBox_Information.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox_Parameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Information
            // 
            this.groupBox_Information.Controls.Add(this.label_Subject);
            this.groupBox_Information.Controls.Add(this.label_Students);
            this.groupBox_Information.Controls.Add(this.label_Activity);
            this.groupBox_Information.Controls.Add(this.label_Group);
            this.groupBox_Information.Controls.Add(this.label_Date);
            this.groupBox_Information.Location = new System.Drawing.Point(774, 12);
            this.groupBox_Information.Name = "groupBox_Information";
            this.groupBox_Information.Size = new System.Drawing.Size(190, 116);
            this.groupBox_Information.TabIndex = 0;
            this.groupBox_Information.TabStop = false;
            this.groupBox_Information.Text = "Information";
            // 
            // label_Subject
            // 
            this.label_Subject.AutoSize = true;
            this.label_Subject.Location = new System.Drawing.Point(6, 47);
            this.label_Subject.Name = "label_Subject";
            this.label_Subject.Size = new System.Drawing.Size(71, 13);
            this.label_Subject.TabIndex = 4;
            this.label_Subject.Text = "label_Subject";
            // 
            // label_Students
            // 
            this.label_Students.AutoSize = true;
            this.label_Students.Location = new System.Drawing.Point(6, 90);
            this.label_Students.Name = "label_Students";
            this.label_Students.Size = new System.Drawing.Size(77, 13);
            this.label_Students.TabIndex = 3;
            this.label_Students.Text = "label_Students";
            // 
            // label_Activity
            // 
            this.label_Activity.AutoSize = true;
            this.label_Activity.Location = new System.Drawing.Point(6, 60);
            this.label_Activity.Name = "label_Activity";
            this.label_Activity.Size = new System.Drawing.Size(69, 13);
            this.label_Activity.TabIndex = 2;
            this.label_Activity.Text = "label_Activity";
            // 
            // label_Group
            // 
            this.label_Group.AutoSize = true;
            this.label_Group.Location = new System.Drawing.Point(6, 34);
            this.label_Group.Name = "label_Group";
            this.label_Group.Size = new System.Drawing.Size(64, 13);
            this.label_Group.TabIndex = 1;
            this.label_Group.Text = "label_Group";
            // 
            // label_Date
            // 
            this.label_Date.AutoSize = true;
            this.label_Date.Location = new System.Drawing.Point(6, 21);
            this.label_Date.Name = "label_Date";
            this.label_Date.Size = new System.Drawing.Size(58, 13);
            this.label_Date.TabIndex = 0;
            this.label_Date.Text = "label_Date";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(756, 525);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.TabStop = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Number";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 30;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Name Surname";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Mark";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 30;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Student\'s E-Mail";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 150;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Parent\'s Name(1)";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 200;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Parent\'s first E-Mail";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 150;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Parent\'s Name(2)";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 200;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Parent\'s second E-Mail";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 150;
            // 
            // groupBox_Parameters
            // 
            this.groupBox_Parameters.Controls.Add(this.checkBoxUploadMarks);
            this.groupBox_Parameters.Controls.Add(this.buttonSendMarks);
            this.groupBox_Parameters.Controls.Add(this.checkBoxSendToParentsSecondEmail);
            this.groupBox_Parameters.Controls.Add(this.checkBoxSendToParentsFirstEmail);
            this.groupBox_Parameters.Controls.Add(this.checkBoxSendToStudents);
            this.groupBox_Parameters.Controls.Add(this.checkBoxSendReport);
            this.groupBox_Parameters.Location = new System.Drawing.Point(774, 134);
            this.groupBox_Parameters.Name = "groupBox_Parameters";
            this.groupBox_Parameters.Size = new System.Drawing.Size(190, 403);
            this.groupBox_Parameters.TabIndex = 2;
            this.groupBox_Parameters.TabStop = false;
            this.groupBox_Parameters.Text = "Parameters";
            // 
            // checkBoxUploadMarks
            // 
            this.checkBoxUploadMarks.AutoSize = true;
            this.checkBoxUploadMarks.Checked = true;
            this.checkBoxUploadMarks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUploadMarks.Location = new System.Drawing.Point(6, 19);
            this.checkBoxUploadMarks.Name = "checkBoxUploadMarks";
            this.checkBoxUploadMarks.Size = new System.Drawing.Size(91, 17);
            this.checkBoxUploadMarks.TabIndex = 5;
            this.checkBoxUploadMarks.Text = "Upload marks";
            this.checkBoxUploadMarks.UseVisualStyleBackColor = true;
            // 
            // buttonSendMarks
            // 
            this.buttonSendMarks.Location = new System.Drawing.Point(6, 374);
            this.buttonSendMarks.Name = "buttonSendMarks";
            this.buttonSendMarks.Size = new System.Drawing.Size(178, 23);
            this.buttonSendMarks.TabIndex = 4;
            this.buttonSendMarks.Text = "Send Marks";
            this.buttonSendMarks.UseVisualStyleBackColor = true;
            this.buttonSendMarks.Click += new System.EventHandler(this.buttonSendMarks_Click);
            // 
            // checkBoxSendToParentsSecondEmail
            // 
            this.checkBoxSendToParentsSecondEmail.AutoSize = true;
            this.checkBoxSendToParentsSecondEmail.Location = new System.Drawing.Point(6, 111);
            this.checkBoxSendToParentsSecondEmail.Name = "checkBoxSendToParentsSecondEmail";
            this.checkBoxSendToParentsSecondEmail.Size = new System.Drawing.Size(168, 17);
            this.checkBoxSendToParentsSecondEmail.TabIndex = 3;
            this.checkBoxSendToParentsSecondEmail.Text = "Send to parent\'s second email";
            this.checkBoxSendToParentsSecondEmail.UseVisualStyleBackColor = true;
            this.checkBoxSendToParentsSecondEmail.CheckedChanged += new System.EventHandler(this.checkBoxIsNecessaryToSave);
            // 
            // checkBoxSendToParentsFirstEmail
            // 
            this.checkBoxSendToParentsFirstEmail.AutoSize = true;
            this.checkBoxSendToParentsFirstEmail.Location = new System.Drawing.Point(6, 88);
            this.checkBoxSendToParentsFirstEmail.Name = "checkBoxSendToParentsFirstEmail";
            this.checkBoxSendToParentsFirstEmail.Size = new System.Drawing.Size(149, 17);
            this.checkBoxSendToParentsFirstEmail.TabIndex = 2;
            this.checkBoxSendToParentsFirstEmail.Text = "Send to parent\'s first email";
            this.checkBoxSendToParentsFirstEmail.UseVisualStyleBackColor = true;
            this.checkBoxSendToParentsFirstEmail.CheckedChanged += new System.EventHandler(this.checkBoxIsNecessaryToSave);
            // 
            // checkBoxSendToStudents
            // 
            this.checkBoxSendToStudents.AutoSize = true;
            this.checkBoxSendToStudents.Location = new System.Drawing.Point(6, 65);
            this.checkBoxSendToStudents.Name = "checkBoxSendToStudents";
            this.checkBoxSendToStudents.Size = new System.Drawing.Size(106, 17);
            this.checkBoxSendToStudents.TabIndex = 1;
            this.checkBoxSendToStudents.Text = "Send to students";
            this.checkBoxSendToStudents.UseVisualStyleBackColor = true;
            this.checkBoxSendToStudents.CheckedChanged += new System.EventHandler(this.checkBoxIsNecessaryToSave);
            // 
            // checkBoxSendReport
            // 
            this.checkBoxSendReport.AutoSize = true;
            this.checkBoxSendReport.Location = new System.Drawing.Point(6, 42);
            this.checkBoxSendReport.Name = "checkBoxSendReport";
            this.checkBoxSendReport.Size = new System.Drawing.Size(81, 17);
            this.checkBoxSendReport.TabIndex = 0;
            this.checkBoxSendReport.Text = "Send report";
            this.checkBoxSendReport.UseVisualStyleBackColor = true;
            this.checkBoxSendReport.CheckedChanged += new System.EventHandler(this.checkBoxIsNecessaryToSave);
            // 
            // Form_Send_Marks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 549);
            this.Controls.Add(this.groupBox_Parameters);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox_Information);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Send_Marks";
            this.Text = "Send marks";
            this.groupBox_Information.ResumeLayout(false);
            this.groupBox_Information.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox_Parameters.ResumeLayout(false);
            this.groupBox_Parameters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Information;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label_Students;
        private System.Windows.Forms.Label label_Activity;
        private System.Windows.Forms.Label label_Group;
        private System.Windows.Forms.Label label_Date;
        private System.Windows.Forms.GroupBox groupBox_Parameters;
        private System.Windows.Forms.CheckBox checkBoxSendReport;
        private System.Windows.Forms.CheckBox checkBoxSendToParentsFirstEmail;
        private System.Windows.Forms.CheckBox checkBoxSendToStudents;
        private System.Windows.Forms.Button buttonSendMarks;
        private System.Windows.Forms.CheckBox checkBoxSendToParentsSecondEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.Label label_Subject;
        private System.Windows.Forms.CheckBox checkBoxUploadMarks;
    }
}