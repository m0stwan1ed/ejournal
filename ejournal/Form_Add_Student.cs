﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Add_Student : Form
    {
        int studentsID;
        string groupName;
        SQLiteConnection databaseConnection; // Подклчение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML
        string language;

        string added;
        string saved;

        public Form_Add_Student(string groupName, SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы для добавления студента
        {
            InitializeComponent();

            this.groupName = groupName;
            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();

            button_save.Visible = false;

            try
            {
                string sqlQuery = "SELECT groupName FROM groups";
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox1.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }

                for(int i = 0; i < comboBox1.Items.Count; i++)
                {
                    if (comboBox1.Items[i].ToString().Equals(groupName))
                    {
                        comboBox1.SelectedItem = comboBox1.Items[i];
                        break;
                    }  
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        
        public Form_Add_Student(int studentsID, SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы для изменения данных о студенте
        {
            InitializeComponent();

            this.studentsID = studentsID;
            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();

            button_add_student.Visible = false;
            button_clear.Visible = false;
            
            try
            {
                string sqlQuery = "SELECT groupName FROM groups";
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox1.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }


                sqlQuery = "SELECT * FROM students WHERE studentsId="+studentsID;
                answerTable = new DataTable();
                adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        textBox_students_surname.Text = answerTable.Rows[0].ItemArray[1].ToString();
                        textBox_students_name.Text = answerTable.Rows[0].ItemArray[2].ToString();
                        textBox_students_patronym.Text = answerTable.Rows[0].ItemArray[3].ToString();
                        textBox_students_phone_number.Text = answerTable.Rows[0].ItemArray[4].ToString();
                        textBox_students_email.Text = answerTable.Rows[0].ItemArray[5].ToString();

                        for (int j = 0; i < comboBox1.Items.Count; i++)
                        {
                            if (comboBox1.Items[j].ToString().Equals(answerTable.Rows[0].ItemArray[6].ToString()))
                            {
                                comboBox1.SelectedItem = comboBox1.Items[j];
                                break;
                            }
                        }

                        textBox_parents_first_surname.Text = answerTable.Rows[0].ItemArray[7].ToString();
                        textBox_parents_first_name.Text = answerTable.Rows[0].ItemArray[8].ToString();
                        textBox_parents_first_patronym.Text = answerTable.Rows[0].ItemArray[9].ToString();
                        textBox_parents_first_phone_number.Text = answerTable.Rows[0].ItemArray[10].ToString();
                        textBox_parents_first_email.Text = answerTable.Rows[0].ItemArray[11].ToString();

                        textBox_parents_second_surname.Text = answerTable.Rows[0].ItemArray[12].ToString();
                        textBox_parents_second_name.Text = answerTable.Rows[0].ItemArray[13].ToString();
                        textBox_parents_second_patronym.Text = answerTable.Rows[0].ItemArray[14].ToString();
                        textBox_parents_second_phone_number.Text = answerTable.Rows[0].ItemArray[15].ToString();
                        textBox_parents_second_email.Text = answerTable.Rows[0].ItemArray[16].ToString();
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        
        void updateLocalization() // Метод обновления локализации формы
        {
            this.Text = localization.Element("localization").Element("addStudent").Element("addStudent").Value;

            label_required_fields.Text = localization.Element("localization").Element("addStudent").Element("fields").Value;

            groupBox1.Text = localization.Element("localization").Element("addStudent").Element("student").Value;
            label_students_surname.Text = localization.Element("localization").Element("addStudent").Element("surname").Value + "* :";
            label_students_name.Text = localization.Element("localization").Element("addStudent").Element("name").Value + "* :";
            label_students_patronym.Text = localization.Element("localization").Element("addStudent").Element("patronym").Value + "* :";
            label_students_phone_number.Text = localization.Element("localization").Element("addStudent").Element("phoneNumber").Value + " :";
            label_students_email.Text = localization.Element("localization").Element("addStudent").Element("eMail").Value + "* :";
            label_students_group.Text = localization.Element("localization").Element("addStudent").Element("group").Value + " :";

            groupBox2.Text = localization.Element("localization").Element("addStudent").Element("parents").Value;
            label_parents_first_surname.Text = localization.Element("localization").Element("addStudent").Element("surname").Value +" :" ;
            label_parents_first_name.Text = localization.Element("localization").Element("addStudent").Element("name").Value + " :";
            label_parents_first_patronym.Text = localization.Element("localization").Element("addStudent").Element("patronym").Value + " :";
            label_parents_first_phone_number.Text = localization.Element("localization").Element("addStudent").Element("phoneNumber").Value + " :";
            label_parents_first_email.Text = localization.Element("localization").Element("addStudent").Element("eMail").Value + " :";

            label_parents_second_surname.Text = localization.Element("localization").Element("addStudent").Element("surname").Value + " :";
            label_parents_second_name.Text = localization.Element("localization").Element("addStudent").Element("name").Value + " :";
            label_parents_second_patronym.Text = localization.Element("localization").Element("addStudent").Element("patronym").Value + " :";
            label_parents_second_phone_number.Text = localization.Element("localization").Element("addStudent").Element("phoneNumber").Value + " :";
            label_parents_second_email.Text = localization.Element("localization").Element("addStudent").Element("eMail").Value + " :";

            button_add_student.Text = localization.Element("localization").Element("addStudent").Element("addStudent").Value;
            button_save.Text = localization.Element("localization").Element("addStudent").Element("save").Value;
            button_clear.Text = localization.Element("localization").Element("addStudent").Element("clear").Value;
            button_cancel.Text = localization.Element("localization").Element("addStudent").Element("cancel").Value;
            
            added = localization.Element("localization").Element("addStudent").Element("added").Value;
            saved = localization.Element("localization").Element("addStudent").Element("saved").Value;
        }

        void clear() // Очистка заполненых полей
        {
            Color install = groupBox1.ForeColor;
            label_required_fields.Visible = false;
            label_students_surname.ForeColor = install;
            label_students_name.ForeColor = install;
            label_students_patronym.ForeColor = install;
            label_students_email.ForeColor = install;

            textBox_students_surname.Clear();
            textBox_students_name.Clear();
            textBox_students_patronym.Clear();
            textBox_students_phone_number.Clear();
            textBox_students_email.Clear();

            textBox_parents_first_surname.Clear();
            textBox_parents_first_name.Clear();
            textBox_parents_first_patronym.Clear();
            textBox_parents_first_phone_number.Clear();
            textBox_parents_first_email.Clear();

            textBox_parents_second_surname.Clear();
            textBox_parents_second_name.Clear();
            textBox_parents_second_patronym.Clear();
            textBox_parents_second_phone_number.Clear();
            textBox_parents_second_email.Clear();
        }

        private void button_clear_Click(object sender, EventArgs e) // Нажание на кнопку "Очистка"
        {
            clear();
        }

        private void button_cancel_Click(object sender, EventArgs e) // Нажатие на кнопку "Отмена"
        {
            this.Close();
        }

        private void button_add_student_Click(object sender, EventArgs e) // Нажатие на кнопку "Добавить студента"
        {
            string studentsGroup = comboBox1.SelectedItem.ToString();
            string studentsSurname = textBox_students_surname.Text;
            string studentsName = textBox_students_name.Text;
            string studentsPatronym = textBox_students_patronym.Text;
            string studentsPhoneNumber = textBox_students_phone_number.Text;
            string studentsEmail = textBox_students_email.Text;

            string parentsFirstSurname = textBox_parents_first_surname.Text;
            string parentsFirstName = textBox_parents_first_name.Text;
            string parentsFirstPatronym = textBox_parents_first_patronym.Text;
            string parentsFirstPhoneNumber = textBox_parents_first_phone_number.Text;
            string parentsFirstEmail = textBox_parents_first_email.Text;

            string parentsSecondSurname = textBox_parents_second_surname.Text;
            string parentsSecondName = textBox_parents_second_name.Text;
            string parentsSecondPatronym = textBox_parents_second_patronym.Text;
            string parentsSecondPhoneNumber = textBox_parents_second_phone_number.Text;
            string parentsSecondEmail = textBox_parents_second_email.Text;

            if ((studentsName.Equals("")) || (studentsEmail.Equals("")) || (studentsSurname.Equals("")) || (studentsPatronym.Equals("")))
            {
                label_required_fields.Visible = true;

                label_students_surname.ForeColor = Color.Red;
                label_students_name.ForeColor = Color.Red;
                label_students_patronym.ForeColor = Color.Red;
                label_students_email.ForeColor = Color.Red;
            }
            else
            {
                // Восстановление цвета сообщений
                Color install = groupBox1.ForeColor;
                label_required_fields.Visible = false;
                label_students_surname.ForeColor = install;
                label_students_name.ForeColor = install;
                label_students_patronym.ForeColor = install;
                label_students_email.ForeColor = install;

                sqlCommand.CommandText = "INSERT INTO students(studentsSurname, studentsName, studentsPatronym, studentsPhoneNumber, studentsEmail, groupName, " +
                    "parentsFirstSurname, parentsFirstName, parentsFirstPatronym, parentsFirstPhoneNumber, parentsFirstEmail, " +
                    "parentsSecondSurname, parentsSecondName, parentsSecondPatronym, parentsSecondPhoneNumber, parentsSecondEmail) " +
                    "VALUES ('" + studentsSurname + "', '" + studentsName + "', '" + studentsPatronym + "', '" + studentsPhoneNumber + "', '" + studentsEmail + "', '" + studentsGroup + "', " +
                    "'" + parentsFirstSurname + "', '" + parentsFirstName + "', '" + parentsFirstPatronym + "', '" + parentsFirstPhoneNumber + "', '" + parentsFirstEmail + "', " +
                    "'" + parentsSecondSurname + "', '" + parentsSecondName + "', '" + parentsSecondPatronym + "', '" + parentsSecondPhoneNumber + "', '" + parentsSecondEmail + "')";
                sqlCommand.ExecuteNonQuery();

                MessageBox.Show(added, added); // Вывод уведомления о добавлении студента в список
                clear();
            }
        }

        private void button_save_Click(object sender, EventArgs e) // Нажатие на кнопку "Сохранить"
        {
            string studentsGroup = comboBox1.SelectedItem.ToString();
            string studentsSurname = textBox_students_surname.Text;
            string studentsName = textBox_students_name.Text;
            string studentsPatronym = textBox_students_patronym.Text;
            string studentsPhoneNumber = textBox_students_phone_number.Text;
            string studentsEmail = textBox_students_email.Text;

            string parentsFirstSurname = textBox_parents_first_surname.Text;
            string parentsFirstName = textBox_parents_first_name.Text;
            string parentsFirstPatronym = textBox_parents_first_patronym.Text;
            string parentsFirstPhoneNumber = textBox_parents_first_phone_number.Text;
            string parentsFirstEmail = textBox_parents_first_email.Text;

            string parentsSecondSurname = textBox_parents_second_surname.Text;
            string parentsSecondName = textBox_parents_second_name.Text;
            string parentsSecondPatronym = textBox_parents_second_patronym.Text;
            string parentsSecondPhoneNumber = textBox_parents_second_phone_number.Text;
            string parentsSecondEmail = textBox_parents_second_email.Text;

            if ((studentsName.Equals("")) || (studentsEmail.Equals("")) || (studentsSurname.Equals("")) || (studentsPatronym.Equals("")))
            {
                label_required_fields.Visible = true;

                label_students_surname.ForeColor = Color.Red;
                label_students_name.ForeColor = Color.Red;
                label_students_patronym.ForeColor = Color.Red;
                label_students_email.ForeColor = Color.Red;
            }
            else
            {
                // Восстановление цвета сообщений
                Color install = groupBox1.ForeColor;
                label_required_fields.Visible = false;
                label_students_surname.ForeColor = install;
                label_students_name.ForeColor = install;
                label_students_patronym.ForeColor = install;
                label_students_email.ForeColor = install;

                sqlCommand.CommandText = "UPDATE students SET " +
                                         "studentsSurname = '" + studentsSurname + "', " +
                                         "studentsName = '" + studentsName + "', " +
                                         "studentsPatronym = '" + studentsPatronym + "', " +
                                         "studentsPhoneNumber = '" + studentsPhoneNumber + "', " +
                                         "studentsEmail = '" + studentsEmail + "'," +
                                         "groupName = '" + comboBox1.SelectedItem.ToString() + "', " +
                                         "parentsFirstSurname = '" + parentsFirstSurname + "', " +
                                         "parentsFirstName = '" + parentsFirstName + "', " +
                                         "parentsFirstPatronym = '" + parentsFirstPatronym + "', " +
                                         "parentsFirstPhoneNumber = '" + parentsFirstPhoneNumber + "', " +
                                         "parentsFirstEmail = '" + parentsFirstEmail + "', " +
                                         "parentsSecondSurname = '" + parentsSecondSurname + "', " +
                                         "parentsSecondName = '" + parentsSecondName + "', " +
                                         "parentsSecondPatronym = '" + parentsSecondPatronym + "', " +
                                         "parentsSecondPhoneNumber = '" + parentsSecondPhoneNumber + "', " +
                                         "parentsSecondEmail = '" + parentsSecondEmail + "' " +
                                         "WHERE studentsId = "+studentsID;
                sqlCommand.ExecuteNonQuery();

                MessageBox.Show(saved, saved); // Вывод уведомления о добавлении студента в список
                this.Close();
            }
        }
    }
}
