﻿namespace ejournal
{
    partial class Form_Manage_Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_manage_students = new System.Windows.Forms.Button();
            this.button_manage_subjects = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_manage_students
            // 
            this.button_manage_students.Image = global::ejournal.Properties.Resources.ejournal_groups;
            this.button_manage_students.Location = new System.Drawing.Point(12, 73);
            this.button_manage_students.Name = "button_manage_students";
            this.button_manage_students.Size = new System.Drawing.Size(145, 55);
            this.button_manage_students.TabIndex = 1;
            this.button_manage_students.Text = "Manage students";
            this.button_manage_students.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_manage_students.UseVisualStyleBackColor = true;
            this.button_manage_students.Click += new System.EventHandler(this.button_manage_students_Click);
            // 
            // button_manage_subjects
            // 
            this.button_manage_subjects.Image = global::ejournal.Properties.Resources.ejournal_subjects;
            this.button_manage_subjects.Location = new System.Drawing.Point(12, 12);
            this.button_manage_subjects.Name = "button_manage_subjects";
            this.button_manage_subjects.Size = new System.Drawing.Size(145, 55);
            this.button_manage_subjects.TabIndex = 0;
            this.button_manage_subjects.Text = "Manage subjects";
            this.button_manage_subjects.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_manage_subjects.UseVisualStyleBackColor = true;
            this.button_manage_subjects.Click += new System.EventHandler(this.button_manage_subjects_Click);
            // 
            // Form_Manage_Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(169, 143);
            this.Controls.Add(this.button_manage_students);
            this.Controls.Add(this.button_manage_subjects);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Manage_Data";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_Manage_Data";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_manage_subjects;
        private System.Windows.Forms.Button button_manage_students;
    }
}