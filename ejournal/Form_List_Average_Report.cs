﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_List_Average_Report : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\";
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml";
        string language = "";
        
        int quantityOfParameters = 0; // Колличество отмеченых параметров

        // Параметры локализации окна
        string localization_averageBySubject = "Average of student by subject";
        string localization_averageByActivity = "Average by subject's activity";
        string localization_chooseGroup;
        string localization_chooseStudent;
        string localization_chooseSubject;
        string localization_chooseActivity;

        string studentsName = "studentsName";
        string group = "group";
        string subjectName = "subjectName";
        string activity = "activity";
        string date = "date";
        string mark = "mark";

        public Form_List_Average_Report(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор окна
        {
            InitializeComponent();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();

            comboBox_type.Items.Add(localization_averageBySubject);
            comboBox_type.Items.Add(localization_averageByActivity);

            // Получение списка групп из базы данных
            string sqlQuery = "SELECT groupName FROM groups ORDER BY groupName";
            DataTable answerTable = new DataTable();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);

            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_group.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }

            // Получение списка предметов из базы данных
            sqlQuery = "SELECT subjectName FROM subjects ORDER BY subjectName";
            answerTable = new DataTable();
            adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_subject.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }
        }

        void updateLocalization() // Метод обновления локализации формы
        {
            this.Text = localization.Element("localization").Element("reports").Element("listAverageReport").Element("listAverageReport").Value;

            groupBox1.Text = localization.Element("localization").Element("reports").Element("listAverageReport").Element("parameters").Value;

            comboBox_type.Text = "[" + localization.Element("localization").Element("reports").Element("listAverageReport").Element("chooseType").Value + "]";
            localization_averageBySubject = localization.Element("localization").Element("reports").Element("listAverageReport").Element("averageBySubject").Value;
            localization_averageByActivity = localization.Element("localization").Element("reports").Element("listAverageReport").Element("averageByActivity").Value;
            comboBox_group.Text = "[" + localization.Element("localization").Element("reports").Element("listAverageReport").Element("chooseGroup").Value + "]";
            localization_chooseGroup = comboBox_group.Text;
            comboBox_student.Text = "[" + localization.Element("localization").Element("reports").Element("listAverageReport").Element("chooseStudent").Value + "]";
            localization_chooseStudent = comboBox_student.Text;
            comboBox_subject.Text = "[" + localization.Element("localization").Element("reports").Element("listAverageReport").Element("chooseSubject").Value + "]";
            localization_chooseSubject = comboBox_subject.Text;
            comboBox_activity.Text = "[" + localization.Element("localization").Element("reports").Element("listAverageReport").Element("chooseActivity").Value + "]";
            localization_chooseActivity = comboBox_activity.Text;

            date = localization.Element("localization").Element("reports").Element("listAverageReport").Element("date").Value;
            checkBox_date.Text = date;
            label_date_from.Text = localization.Element("localization").Element("reports").Element("listAverageReport").Element("from").Value + " :";
            label_date_to.Text = localization.Element("localization").Element("reports").Element("listAverageReport").Element("to").Value + " :";
            button_get_report.Text = localization.Element("localization").Element("reports").Element("listAverageReport").Element("getReport").Value;
            studentsName = localization.Element("localization").Element("reports").Element("listAverageReport").Element("studentsName").Value;
            group = localization.Element("localization").Element("reports").Element("listAverageReport").Element("group").Value;
            subjectName = localization.Element("localization").Element("reports").Element("listAverageReport").Element("subjectName").Value;
            mark = localization.Element("localization").Element("reports").Element("listAverageReport").Element("mark").Value;
            activity = localization.Element("localization").Element("reports").Element("listAverageReport").Element("activity").Value;
        }

        private void comboBox_type_SelectedIndexChanged(object sender, EventArgs e) // При изменении типа запроса изменяются поля для ввода данных
        {
            comboBox_group.SelectedItem = null;
            comboBox_student.SelectedItem = null;
            comboBox_subject.SelectedItem = null;
            comboBox_activity.SelectedItem = null;
            comboBox_group.Text = localization_chooseGroup;
            comboBox_student.Text = localization_chooseStudent;
            comboBox_subject.Text = localization_chooseSubject;
            comboBox_activity.Text = localization_chooseActivity;

            if (comboBox_type.SelectedIndex == 0)
            {
                comboBox_group.Enabled = true;
                comboBox_student.Enabled = true;
                comboBox_subject.Enabled = true;
                comboBox_activity.Enabled = false;
                comboBox_activity.SelectedItem = null;
                comboBox_activity.Items.Clear();
            }

            if(comboBox_type.SelectedIndex == 1)
            {
                comboBox_group.Enabled = true;
                comboBox_group.SelectedItem = null;
                comboBox_student.Enabled = false;
                comboBox_student.SelectedItem = null;
                comboBox_student.Items.Clear();
                comboBox_subject.Enabled = true;
                comboBox_activity.Enabled = true;
            }
        }

        private void checkBox_date_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_date.Checked)
            {
                label_date_from.Enabled = true;
                label_date_to.Enabled = true;
                dateTimePicker_from.Enabled = true;
                dateTimePicker_to.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                label_date_from.Enabled = false;
                label_date_to.Enabled = false;
                dateTimePicker_from.Enabled = false;
                dateTimePicker_to.Enabled = false;
                quantityOfParameters--;
            }
        } // Если отмечена дата

        private void comboBox_group_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_group.SelectedItem != null)
            {
                string sqlQuery = "SELECT (studentssurname || ' ' || studentsName || ' ' || studentsPatronym) AS studentsName " +
                                  "FROM students " +
                                  "WHERE groupName = '" + comboBox_group.SelectedItem.ToString() + "'" +
                                  "ORDER BY studentsName";

                comboBox_student.Items.Clear();

                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox_student.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }
            }
        } // Если выбрана группа перезагрузить сисок студентов

        private void comboBox_subject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_subject.SelectedItem != null)
            {
                string sqlQuery = "SELECT activity " +
                                  "FROM marks " +
                                  "WHERE subjectName = '" + comboBox_subject.SelectedItem.ToString() + "' " +
                                  "GROUP BY activity " +
                                  "ORDER BY activity";

                comboBox_activity.Items.Clear();

                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);
                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox_activity.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }
            }
        } // Если выбран предмет перезагрузить ссписок активностей

        private void button_get_report_Click(object sender, EventArgs e) // Нажатие на кнопку "Получить отчёт"
        {
            DataTable answerTable = new DataTable(); // Ответ базы данных в виде таблицы

            if((comboBox_type.SelectedIndex == 0)&&(comboBox_group.SelectedItem != null)&&(comboBox_student.SelectedItem != null)&&(comboBox_subject.SelectedItem != null)) // Если поле не отмечено то запрос не выполнится
            {
                string dateFrom = dateTimePicker_from.Value.Date.ToString("yyyy-MM-dd");
                string dateTo = dateTimePicker_to.Value.Date.ToString("yyyy-MM-dd");

                string sqlQuery = "SELECT (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym) AS '" + studentsName + "', groupName AS '" + group + "', subjectName AS '" + subjectName + "', " +
                                  " AVG(mark) AS '" + mark + "'" +
                                  " FROM students LEFT OUTER JOIN marks USING(studentsId) " +
                                  " WHERE subjectName = '" + comboBox_subject.SelectedItem.ToString() + "'" +
                                  " AND (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym) = '" + comboBox_student.SelectedItem.ToString() + "'";

                if(checkBox_date.Checked)
                {
                    sqlQuery += "AND date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                }
                                  

                sqlQuery += " GROUP BY (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym)";
                sqlQuery += " ORDER BY\"" + studentsName + "\"";

                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                dataGridView1.DataSource = answerTable;
                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }

            if((comboBox_type.SelectedIndex == 1)&& (comboBox_group.SelectedItem != null) && (comboBox_activity.SelectedItem != null) && (comboBox_subject.SelectedItem != null)) // Если поле не отмечено то запрос не выполнится
            {
                string dateFrom = dateTimePicker_from.Value.Day + "." + dateTimePicker_from.Value.Month + "." + dateTimePicker_from.Value.Year;
                string dateTo = dateTimePicker_to.Value.Day + "." + dateTimePicker_to.Value.Month + "." + dateTimePicker_to.Value.Year;

                string sqlQuery = "SELECT groupName AS '" + group + "', subjectName AS '" + subjectName + "', activity AS '" + activity + "', date AS '" + date +"', "+
                                  " AVG(mark) AS '" + mark + "'" +
                                  " FROM students LEFT OUTER JOIN marks USING(studentsId) " +
                                  " WHERE subjectName = '" + comboBox_subject.SelectedItem.ToString() + "'" +
                                  " AND groupName = '" + comboBox_group.SelectedItem.ToString() + "'" +
                                  " AND activity = '" + comboBox_activity.SelectedItem.ToString() + "'";

                if (checkBox_date.Checked)
                {
                    sqlQuery += "AND date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                }
                sqlQuery += " GROUP BY date, groupName, subjectName, activity ORDER BY date";

                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                dataGridView1.DataSource = answerTable;
                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }
        } 
    }
}
