﻿namespace ejournal
{
    partial class Form_Add_Student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox_students_email = new System.Windows.Forms.TextBox();
            this.textBox_students_phone_number = new System.Windows.Forms.TextBox();
            this.textBox_students_patronym = new System.Windows.Forms.TextBox();
            this.textBox_students_name = new System.Windows.Forms.TextBox();
            this.textBox_students_surname = new System.Windows.Forms.TextBox();
            this.label_students_group = new System.Windows.Forms.Label();
            this.label_students_email = new System.Windows.Forms.Label();
            this.label_students_phone_number = new System.Windows.Forms.Label();
            this.label_students_patronym = new System.Windows.Forms.Label();
            this.label_students_name = new System.Windows.Forms.Label();
            this.label_students_surname = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_parents_second_email = new System.Windows.Forms.TextBox();
            this.textBox_parents_second_phone_number = new System.Windows.Forms.TextBox();
            this.textBox_parents_second_patronym = new System.Windows.Forms.TextBox();
            this.textBox_parents_second_name = new System.Windows.Forms.TextBox();
            this.textBox_parents_second_surname = new System.Windows.Forms.TextBox();
            this.label_parents_second_email = new System.Windows.Forms.Label();
            this.label_parents_second_phone_number = new System.Windows.Forms.Label();
            this.label_parents_second_patronym = new System.Windows.Forms.Label();
            this.label_parents_second_name = new System.Windows.Forms.Label();
            this.label_parents_second_surname = new System.Windows.Forms.Label();
            this.textBox_parents_first_email = new System.Windows.Forms.TextBox();
            this.textBox_parents_first_phone_number = new System.Windows.Forms.TextBox();
            this.textBox_parents_first_patronym = new System.Windows.Forms.TextBox();
            this.textBox_parents_first_name = new System.Windows.Forms.TextBox();
            this.textBox_parents_first_surname = new System.Windows.Forms.TextBox();
            this.label_parents_first_email = new System.Windows.Forms.Label();
            this.label_parents_first_phone_number = new System.Windows.Forms.Label();
            this.label_parents_first_patronym = new System.Windows.Forms.Label();
            this.label_parents_first_name = new System.Windows.Forms.Label();
            this.label_parents_first_surname = new System.Windows.Forms.Label();
            this.label_required_fields = new System.Windows.Forms.Label();
            this.button_add_student = new System.Windows.Forms.Button();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.textBox_students_email);
            this.groupBox1.Controls.Add(this.textBox_students_phone_number);
            this.groupBox1.Controls.Add(this.textBox_students_patronym);
            this.groupBox1.Controls.Add(this.textBox_students_name);
            this.groupBox1.Controls.Add(this.textBox_students_surname);
            this.groupBox1.Controls.Add(this.label_students_group);
            this.groupBox1.Controls.Add(this.label_students_email);
            this.groupBox1.Controls.Add(this.label_students_phone_number);
            this.groupBox1.Controls.Add(this.label_students_patronym);
            this.groupBox1.Controls.Add(this.label_students_name);
            this.groupBox1.Controls.Add(this.label_students_surname);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 176);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(176, 149);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(246, 21);
            this.comboBox1.TabIndex = 10;
            // 
            // textBox_students_email
            // 
            this.textBox_students_email.Location = new System.Drawing.Point(176, 123);
            this.textBox_students_email.Name = "textBox_students_email";
            this.textBox_students_email.Size = new System.Drawing.Size(246, 20);
            this.textBox_students_email.TabIndex = 9;
            // 
            // textBox_students_phone_number
            // 
            this.textBox_students_phone_number.Location = new System.Drawing.Point(176, 97);
            this.textBox_students_phone_number.Name = "textBox_students_phone_number";
            this.textBox_students_phone_number.Size = new System.Drawing.Size(246, 20);
            this.textBox_students_phone_number.TabIndex = 8;
            // 
            // textBox_students_patronym
            // 
            this.textBox_students_patronym.Location = new System.Drawing.Point(176, 71);
            this.textBox_students_patronym.Name = "textBox_students_patronym";
            this.textBox_students_patronym.Size = new System.Drawing.Size(246, 20);
            this.textBox_students_patronym.TabIndex = 7;
            // 
            // textBox_students_name
            // 
            this.textBox_students_name.Location = new System.Drawing.Point(176, 45);
            this.textBox_students_name.Name = "textBox_students_name";
            this.textBox_students_name.Size = new System.Drawing.Size(246, 20);
            this.textBox_students_name.TabIndex = 6;
            // 
            // textBox_students_surname
            // 
            this.textBox_students_surname.Location = new System.Drawing.Point(176, 19);
            this.textBox_students_surname.Name = "textBox_students_surname";
            this.textBox_students_surname.Size = new System.Drawing.Size(246, 20);
            this.textBox_students_surname.TabIndex = 5;
            // 
            // label_students_group
            // 
            this.label_students_group.AutoSize = true;
            this.label_students_group.Location = new System.Drawing.Point(6, 152);
            this.label_students_group.Name = "label_students_group";
            this.label_students_group.Size = new System.Drawing.Size(42, 13);
            this.label_students_group.TabIndex = 4;
            this.label_students_group.Text = "Group :";
            // 
            // label_students_email
            // 
            this.label_students_email.AutoSize = true;
            this.label_students_email.Location = new System.Drawing.Point(6, 126);
            this.label_students_email.Name = "label_students_email";
            this.label_students_email.Size = new System.Drawing.Size(46, 13);
            this.label_students_email.TabIndex = 3;
            this.label_students_email.Text = "E-Mail* :";
            // 
            // label_students_phone_number
            // 
            this.label_students_phone_number.AutoSize = true;
            this.label_students_phone_number.Location = new System.Drawing.Point(6, 100);
            this.label_students_phone_number.Name = "label_students_phone_number";
            this.label_students_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_students_phone_number.TabIndex = 1;
            this.label_students_phone_number.Text = "Phone number :";
            // 
            // label_students_patronym
            // 
            this.label_students_patronym.AutoSize = true;
            this.label_students_patronym.Location = new System.Drawing.Point(6, 74);
            this.label_students_patronym.Name = "label_students_patronym";
            this.label_students_patronym.Size = new System.Drawing.Size(61, 13);
            this.label_students_patronym.TabIndex = 2;
            this.label_students_patronym.Text = "Patronym* :";
            // 
            // label_students_name
            // 
            this.label_students_name.AutoSize = true;
            this.label_students_name.Location = new System.Drawing.Point(6, 48);
            this.label_students_name.Name = "label_students_name";
            this.label_students_name.Size = new System.Drawing.Size(45, 13);
            this.label_students_name.TabIndex = 1;
            this.label_students_name.Text = "Name* :";
            // 
            // label_students_surname
            // 
            this.label_students_surname.AutoSize = true;
            this.label_students_surname.Location = new System.Drawing.Point(6, 22);
            this.label_students_surname.Name = "label_students_surname";
            this.label_students_surname.Size = new System.Drawing.Size(59, 13);
            this.label_students_surname.TabIndex = 0;
            this.label_students_surname.Text = "Surname* :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_parents_second_email);
            this.groupBox2.Controls.Add(this.textBox_parents_second_phone_number);
            this.groupBox2.Controls.Add(this.textBox_parents_second_patronym);
            this.groupBox2.Controls.Add(this.textBox_parents_second_name);
            this.groupBox2.Controls.Add(this.textBox_parents_second_surname);
            this.groupBox2.Controls.Add(this.label_parents_second_email);
            this.groupBox2.Controls.Add(this.label_parents_second_phone_number);
            this.groupBox2.Controls.Add(this.label_parents_second_patronym);
            this.groupBox2.Controls.Add(this.label_parents_second_name);
            this.groupBox2.Controls.Add(this.label_parents_second_surname);
            this.groupBox2.Controls.Add(this.textBox_parents_first_email);
            this.groupBox2.Controls.Add(this.textBox_parents_first_phone_number);
            this.groupBox2.Controls.Add(this.textBox_parents_first_patronym);
            this.groupBox2.Controls.Add(this.textBox_parents_first_name);
            this.groupBox2.Controls.Add(this.textBox_parents_first_surname);
            this.groupBox2.Controls.Add(this.label_parents_first_email);
            this.groupBox2.Controls.Add(this.label_parents_first_phone_number);
            this.groupBox2.Controls.Add(this.label_parents_first_patronym);
            this.groupBox2.Controls.Add(this.label_parents_first_name);
            this.groupBox2.Controls.Add(this.label_parents_first_surname);
            this.groupBox2.Location = new System.Drawing.Point(12, 207);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(428, 281);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parents";
            // 
            // textBox_parents_second_email
            // 
            this.textBox_parents_second_email.Location = new System.Drawing.Point(176, 253);
            this.textBox_parents_second_email.Name = "textBox_parents_second_email";
            this.textBox_parents_second_email.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_second_email.TabIndex = 25;
            // 
            // textBox_parents_second_phone_number
            // 
            this.textBox_parents_second_phone_number.Location = new System.Drawing.Point(176, 227);
            this.textBox_parents_second_phone_number.Name = "textBox_parents_second_phone_number";
            this.textBox_parents_second_phone_number.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_second_phone_number.TabIndex = 24;
            // 
            // textBox_parents_second_patronym
            // 
            this.textBox_parents_second_patronym.Location = new System.Drawing.Point(176, 201);
            this.textBox_parents_second_patronym.Name = "textBox_parents_second_patronym";
            this.textBox_parents_second_patronym.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_second_patronym.TabIndex = 23;
            // 
            // textBox_parents_second_name
            // 
            this.textBox_parents_second_name.Location = new System.Drawing.Point(176, 175);
            this.textBox_parents_second_name.Name = "textBox_parents_second_name";
            this.textBox_parents_second_name.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_second_name.TabIndex = 22;
            // 
            // textBox_parents_second_surname
            // 
            this.textBox_parents_second_surname.Location = new System.Drawing.Point(176, 149);
            this.textBox_parents_second_surname.Name = "textBox_parents_second_surname";
            this.textBox_parents_second_surname.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_second_surname.TabIndex = 21;
            // 
            // label_parents_second_email
            // 
            this.label_parents_second_email.AutoSize = true;
            this.label_parents_second_email.Location = new System.Drawing.Point(6, 256);
            this.label_parents_second_email.Name = "label_parents_second_email";
            this.label_parents_second_email.Size = new System.Drawing.Size(42, 13);
            this.label_parents_second_email.TabIndex = 20;
            this.label_parents_second_email.Text = "E-Mail :";
            // 
            // label_parents_second_phone_number
            // 
            this.label_parents_second_phone_number.AutoSize = true;
            this.label_parents_second_phone_number.Location = new System.Drawing.Point(6, 230);
            this.label_parents_second_phone_number.Name = "label_parents_second_phone_number";
            this.label_parents_second_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_parents_second_phone_number.TabIndex = 19;
            this.label_parents_second_phone_number.Text = "Phone number :";
            // 
            // label_parents_second_patronym
            // 
            this.label_parents_second_patronym.AutoSize = true;
            this.label_parents_second_patronym.Location = new System.Drawing.Point(6, 204);
            this.label_parents_second_patronym.Name = "label_parents_second_patronym";
            this.label_parents_second_patronym.Size = new System.Drawing.Size(57, 13);
            this.label_parents_second_patronym.TabIndex = 18;
            this.label_parents_second_patronym.Text = "Patronym :";
            // 
            // label_parents_second_name
            // 
            this.label_parents_second_name.AutoSize = true;
            this.label_parents_second_name.Location = new System.Drawing.Point(6, 178);
            this.label_parents_second_name.Name = "label_parents_second_name";
            this.label_parents_second_name.Size = new System.Drawing.Size(41, 13);
            this.label_parents_second_name.TabIndex = 17;
            this.label_parents_second_name.Text = "Name :";
            // 
            // label_parents_second_surname
            // 
            this.label_parents_second_surname.AutoSize = true;
            this.label_parents_second_surname.Location = new System.Drawing.Point(6, 152);
            this.label_parents_second_surname.Name = "label_parents_second_surname";
            this.label_parents_second_surname.Size = new System.Drawing.Size(55, 13);
            this.label_parents_second_surname.TabIndex = 16;
            this.label_parents_second_surname.Text = "Surname :";
            // 
            // textBox_parents_first_email
            // 
            this.textBox_parents_first_email.Location = new System.Drawing.Point(176, 123);
            this.textBox_parents_first_email.Name = "textBox_parents_first_email";
            this.textBox_parents_first_email.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_first_email.TabIndex = 15;
            // 
            // textBox_parents_first_phone_number
            // 
            this.textBox_parents_first_phone_number.Location = new System.Drawing.Point(176, 97);
            this.textBox_parents_first_phone_number.Name = "textBox_parents_first_phone_number";
            this.textBox_parents_first_phone_number.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_first_phone_number.TabIndex = 14;
            // 
            // textBox_parents_first_patronym
            // 
            this.textBox_parents_first_patronym.Location = new System.Drawing.Point(176, 71);
            this.textBox_parents_first_patronym.Name = "textBox_parents_first_patronym";
            this.textBox_parents_first_patronym.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_first_patronym.TabIndex = 13;
            // 
            // textBox_parents_first_name
            // 
            this.textBox_parents_first_name.Location = new System.Drawing.Point(176, 45);
            this.textBox_parents_first_name.Name = "textBox_parents_first_name";
            this.textBox_parents_first_name.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_first_name.TabIndex = 12;
            // 
            // textBox_parents_first_surname
            // 
            this.textBox_parents_first_surname.Location = new System.Drawing.Point(176, 19);
            this.textBox_parents_first_surname.Name = "textBox_parents_first_surname";
            this.textBox_parents_first_surname.Size = new System.Drawing.Size(246, 20);
            this.textBox_parents_first_surname.TabIndex = 11;
            // 
            // label_parents_first_email
            // 
            this.label_parents_first_email.AutoSize = true;
            this.label_parents_first_email.Location = new System.Drawing.Point(6, 126);
            this.label_parents_first_email.Name = "label_parents_first_email";
            this.label_parents_first_email.Size = new System.Drawing.Size(42, 13);
            this.label_parents_first_email.TabIndex = 4;
            this.label_parents_first_email.Text = "E-Mail :";
            // 
            // label_parents_first_phone_number
            // 
            this.label_parents_first_phone_number.AutoSize = true;
            this.label_parents_first_phone_number.Location = new System.Drawing.Point(6, 100);
            this.label_parents_first_phone_number.Name = "label_parents_first_phone_number";
            this.label_parents_first_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_parents_first_phone_number.TabIndex = 3;
            this.label_parents_first_phone_number.Text = "Phone number :";
            // 
            // label_parents_first_patronym
            // 
            this.label_parents_first_patronym.AutoSize = true;
            this.label_parents_first_patronym.Location = new System.Drawing.Point(6, 74);
            this.label_parents_first_patronym.Name = "label_parents_first_patronym";
            this.label_parents_first_patronym.Size = new System.Drawing.Size(57, 13);
            this.label_parents_first_patronym.TabIndex = 2;
            this.label_parents_first_patronym.Text = "Patronym :";
            // 
            // label_parents_first_name
            // 
            this.label_parents_first_name.AutoSize = true;
            this.label_parents_first_name.Location = new System.Drawing.Point(6, 48);
            this.label_parents_first_name.Name = "label_parents_first_name";
            this.label_parents_first_name.Size = new System.Drawing.Size(41, 13);
            this.label_parents_first_name.TabIndex = 1;
            this.label_parents_first_name.Text = "Name :";
            // 
            // label_parents_first_surname
            // 
            this.label_parents_first_surname.AutoSize = true;
            this.label_parents_first_surname.Location = new System.Drawing.Point(6, 22);
            this.label_parents_first_surname.Name = "label_parents_first_surname";
            this.label_parents_first_surname.Size = new System.Drawing.Size(55, 13);
            this.label_parents_first_surname.TabIndex = 0;
            this.label_parents_first_surname.Text = "Surname :";
            // 
            // label_required_fields
            // 
            this.label_required_fields.ForeColor = System.Drawing.Color.Red;
            this.label_required_fields.Location = new System.Drawing.Point(177, 9);
            this.label_required_fields.Name = "label_required_fields";
            this.label_required_fields.Size = new System.Drawing.Size(263, 13);
            this.label_required_fields.TabIndex = 46;
            this.label_required_fields.Text = "Fields marked by * are required to fill";
            this.label_required_fields.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_required_fields.Visible = false;
            // 
            // button_add_student
            // 
            this.button_add_student.Location = new System.Drawing.Point(12, 494);
            this.button_add_student.Name = "button_add_student";
            this.button_add_student.Size = new System.Drawing.Size(139, 23);
            this.button_add_student.TabIndex = 47;
            this.button_add_student.Text = "Add student";
            this.button_add_student.UseVisualStyleBackColor = true;
            this.button_add_student.Click += new System.EventHandler(this.button_add_student_Click);
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(157, 494);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(139, 23);
            this.button_clear.TabIndex = 48;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(302, 494);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(139, 23);
            this.button_cancel.TabIndex = 49;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(12, 494);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(139, 23);
            this.button_save.TabIndex = 26;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // Form_Add_Student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 526);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.button_add_student);
            this.Controls.Add(this.label_required_fields);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Add_Student";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_Add_Student";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_students_surname;
        private System.Windows.Forms.Label label_students_name;
        private System.Windows.Forms.Label label_students_patronym;
        private System.Windows.Forms.Label label_students_phone_number;
        private System.Windows.Forms.Label label_students_email;
        private System.Windows.Forms.Label label_students_group;
        private System.Windows.Forms.TextBox textBox_students_name;
        private System.Windows.Forms.TextBox textBox_students_surname;
        private System.Windows.Forms.TextBox textBox_students_patronym;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox_students_email;
        private System.Windows.Forms.TextBox textBox_students_phone_number;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_parents_first_surname;
        private System.Windows.Forms.Label label_parents_first_patronym;
        private System.Windows.Forms.Label label_parents_first_name;
        private System.Windows.Forms.TextBox textBox_parents_first_surname;
        private System.Windows.Forms.Label label_parents_first_email;
        private System.Windows.Forms.Label label_parents_first_phone_number;
        private System.Windows.Forms.TextBox textBox_parents_first_email;
        private System.Windows.Forms.TextBox textBox_parents_first_phone_number;
        private System.Windows.Forms.TextBox textBox_parents_first_patronym;
        private System.Windows.Forms.TextBox textBox_parents_first_name;
        private System.Windows.Forms.Label label_parents_second_patronym;
        private System.Windows.Forms.Label label_parents_second_name;
        private System.Windows.Forms.Label label_parents_second_surname;
        private System.Windows.Forms.TextBox textBox_parents_second_email;
        private System.Windows.Forms.TextBox textBox_parents_second_phone_number;
        private System.Windows.Forms.TextBox textBox_parents_second_patronym;
        private System.Windows.Forms.TextBox textBox_parents_second_name;
        private System.Windows.Forms.TextBox textBox_parents_second_surname;
        private System.Windows.Forms.Label label_parents_second_email;
        private System.Windows.Forms.Label label_parents_second_phone_number;
        private System.Windows.Forms.Label label_required_fields;
        private System.Windows.Forms.Button button_add_student;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_save;
    }
}