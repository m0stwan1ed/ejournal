﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;


namespace ejournal
{
    public partial class Form_Manage_Students : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных
        string sqlQuery; // Текст запроса к базе данных
        DataTable answerTable; // Ответ базы данных в виде таблицы
        List<int> studentsIds = new List<int>(); // Список идентификаторов студентов

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML
        string language = "";
        string removeGroup = "";
        string removeGroupConfirmation = "";
        string removeStudent = "";
        string removeStudentConfirmation = "";

        public Form_Manage_Students(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization(); // Обновление локализации формы

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            //Получение списка групп из базы данных
            try
            {
                sqlQuery = "SELECT groupName FROM groups";
                DataTable answerTable = new DataTable();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        comboBox1.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        void updateLocalization()
        {
            this.Text = localization.Element("localization").Element("manageStudents").Element("manageStudents").Value;
            comboBox1.Text = "[" + localization.Element("localization").Element("manageStudents").Element("chooseGroup").Value +  "]";

            button_add_group.Text = localization.Element("localization").Element("manageStudents").Element("addGroup").Value;
            button_remove_group.Text = localization.Element("localization").Element("manageStudents").Element("removeGroup").Value;
            removeGroup = button_remove_group.Text;
            button_add_student.Text = localization.Element("localization").Element("manageStudents").Element("addStudent").Value;
            button_remove_student.Text = localization.Element("localization").Element("manageStudents").Element("removeStudent").Value;
            removeStudent = button_remove_student.Text;

            removeGroupConfirmation = localization.Element("localization").Element("manageStudents").Element("removeGroupConfirmation").Value;
            removeStudentConfirmation = localization.Element("localization").Element("manageStudents").Element("removeStudentConfirmation").Value;
        } // Метод обновления локализации формы
        
        void reloadListBox() // Процедура, что выполняет обновление списка студентов
        {
            listBox1.SelectedItem = null; // Снятие выбора в листБоксе

            answerTable = new DataTable();
            sqlQuery = "SELECT studentsId, studentsSurname || ' ' || studentsName || ' ' || studentsPatronym FROM students " +
                "WHERE groupName='" + comboBox1.SelectedItem.ToString() + "' " +
                "ORDER BY studentsSurname, studentsName, studentsPatronym";
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            listBox1.Items.Clear(); // Очистка листБокса
            studentsIds.Clear();
            if (answerTable.Rows.Count > 0)
            {
                // Добавление студентов из списка в листБокс
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    listBox1.Items.Add(answerTable.Rows[i].ItemArray[1]);
                    studentsIds.Add(Convert.ToInt32(answerTable.Rows[i].ItemArray[0]));
                }
            }
        }

        private void button_add_group_Click(object sender, EventArgs e) // Нажатие на кнопку "Добавить группу"
        {
            string input = Microsoft.VisualBasic.Interaction.InputBox("Input name of new group", "New group", "", -1, -1); // Вызов инпутБокса из VisualBasic и получение ввода пользователя

            if(input.Length != 0) // Если длинна ввода не 0
            {
                sqlCommand.CommandText = "INSERT INTO groups (groupName) VALUES ('" + input + "')";
                sqlCommand.ExecuteNonQuery();
                comboBox1.Items.Add(input); // Добавить группу в выпаающий список
                if (comboBox1.Items.Count == 1) comboBox1.SelectedIndex = 0;
            }
        }

        private void button_remove_group_Click(object sender, EventArgs e) // Нажатие на кнопку "Удалить группу"
        {
            DialogResult dialogRequest = MessageBox.Show(removeGroupConfirmation + " '" + comboBox1.SelectedItem.ToString() + "'?", removeGroup, MessageBoxButtons.YesNo); // Вывод на экран диалогового окна с фопросом об удалении группы
            try
            {
                if (dialogRequest == DialogResult.Yes) // Если результат диалога "Да"
                {
                    sqlCommand.CommandText = "DELETE FROM groups WHERE groupName='" + comboBox1.SelectedItem.ToString() + "'";
                    sqlCommand.ExecuteNonQuery();
                    comboBox1.Items.RemoveAt(comboBox1.SelectedIndex); // Убрать группу из списка
                }
            }
            catch(System.Data.SQLite.SQLiteException)
            {
                MessageBox.Show("Can't remove group: not empty");
            }
            
        }

        private void button_add_student_Click(object sender, EventArgs e) // Нажатие на кнопку "Добавить нового студента"
        {
            if(comboBox1.SelectedItem == null) // Если группа не выбрана
            {
                MessageBox.Show("Please select the group first"); // Вывод диалогового окна с просьой выбрать группу
            }
            else
            {
                Form_Add_Student formAddStudent = new Form_Add_Student(comboBox1.SelectedItem.ToString(), databaseConnection, sqlCommand);
                formAddStudent.ShowDialog();
                formAddStudent.Dispose();
                reloadListBox();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) // Обработка изменения выбора группы
        {
            reloadListBox(); // Обновление списка студентов в листБоксе
        }

        private void button_remove_student_Click(object sender, EventArgs e) // Нажатие на кнопку удалит студента
        {
            // Считывание имени студента
            string selectedStudent = listBox1.SelectedItem.ToString();

            DialogResult dialogRequest = MessageBox.Show(removeStudentConfirmation + " '" + selectedStudent + "'?", removeStudent, MessageBoxButtons.YesNo); // Вопрос о согласии на удаление студента

            if (dialogRequest == DialogResult.Yes) // Если "Да"
            {
                int index = listBox1.SelectedIndex; // Считывание момера студента в списке
                try
                {
                    sqlCommand.CommandText = "DELETE FROM students WHERE studentsID=" + studentsIds[index];
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
                reloadListBox(); // Обновление списка студентов в листБоксе
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e) // Двойное нажатие на список студентов
        {
            if (listBox1.SelectedItem != null) // Если номер не пуст
            {
                int index = listBox1.SelectedIndex; // Считывание момера студента в списке

                Form_ShowStudentsData studentsData = new Form_ShowStudentsData(studentsIds[index], databaseConnection, sqlCommand);
                studentsData.ShowDialog();
                studentsData.Dispose();
                reloadListBox();
            }
        }
    }
}