﻿namespace ejournal
{
    partial class Form_Generate_Excel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_group = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label_activity = new System.Windows.Forms.Label();
            this.textBox_activity = new System.Windows.Forms.TextBox();
            this.label_date = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button_generate = new System.Windows.Forms.Button();
            this.label_Subject = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label_group
            // 
            this.label_group.AutoSize = true;
            this.label_group.Location = new System.Drawing.Point(12, 9);
            this.label_group.Name = "label_group";
            this.label_group.Size = new System.Drawing.Size(42, 13);
            this.label_group.TabIndex = 1;
            this.label_group.Text = "Group :";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 25);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(246, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Text = "[Choose group]";
            // 
            // label_activity
            // 
            this.label_activity.AutoSize = true;
            this.label_activity.Location = new System.Drawing.Point(12, 129);
            this.label_activity.Name = "label_activity";
            this.label_activity.Size = new System.Drawing.Size(47, 13);
            this.label_activity.TabIndex = 3;
            this.label_activity.Text = "Activity :";
            // 
            // textBox_activity
            // 
            this.textBox_activity.Location = new System.Drawing.Point(12, 145);
            this.textBox_activity.Name = "textBox_activity";
            this.textBox_activity.Size = new System.Drawing.Size(246, 20);
            this.textBox_activity.TabIndex = 4;
            // 
            // label_date
            // 
            this.label_date.AutoSize = true;
            this.label_date.Location = new System.Drawing.Point(12, 190);
            this.label_date.Name = "label_date";
            this.label_date.Size = new System.Drawing.Size(36, 13);
            this.label_date.TabIndex = 5;
            this.label_date.Text = "Date :";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 206);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(246, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // button_generate
            // 
            this.button_generate.Location = new System.Drawing.Point(70, 232);
            this.button_generate.Name = "button_generate";
            this.button_generate.Size = new System.Drawing.Size(134, 53);
            this.button_generate.TabIndex = 0;
            this.button_generate.Text = "Generate";
            this.button_generate.UseVisualStyleBackColor = true;
            this.button_generate.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // label_Subject
            // 
            this.label_Subject.AutoSize = true;
            this.label_Subject.Location = new System.Drawing.Point(12, 68);
            this.label_Subject.Name = "label_Subject";
            this.label_Subject.Size = new System.Drawing.Size(49, 13);
            this.label_Subject.TabIndex = 7;
            this.label_Subject.Text = "Subject :";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(12, 84);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(246, 21);
            this.comboBox2.TabIndex = 8;
            this.comboBox2.Text = "[Choose subject]";
            // 
            // Form_Generate_Excel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 290);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label_Subject);
            this.Controls.Add(this.button_generate);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label_date);
            this.Controls.Add(this.textBox_activity);
            this.Controls.Add(this.label_activity);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label_group);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Generate_Excel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_Generate_Excel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_group;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label_activity;
        private System.Windows.Forms.TextBox textBox_activity;
        private System.Windows.Forms.Label label_date;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button_generate;
        private System.Windows.Forms.Label label_Subject;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}