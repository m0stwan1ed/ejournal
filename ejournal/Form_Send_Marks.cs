﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

//Библиотека для работы с Excel
using Excel = Microsoft.Office.Interop.Excel;

// Библиотеки для работы с GMail APi
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_Send_Marks : Form
    {
        SQLiteConnection databaseConnection; // Соединение с базой данных
        SQLiteCommand sqlCommand; // Запрос к базе данных
        string sqlQuery; // Текст запроса к базе данных
        DataTable answerTable; // Ответ базы данных в виде таблицы

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес папки файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек приложения

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML
        string language = ""; // Язык приложения

        // Параметры локализации
        string numberLocalization = "";
        string nameLocalization = "";
        string markLocalization = "";
        string dateLocalization = "";
        string groupLocalization = "";
        string subjectLocalizaion = "";
        string activityLocalization = "";
        string studentsLocalization = "";

        string reportSubject = "";
        string reportBody1 = "";
        string reportBody2 = "";
        string reportBody3 = "";
        string reportBody4 = "";
        string reportBody5 = "";
        string reportBody6 = "";
        string reportSent = "";
        string reportError = "";

        string performanceSubject = "";
        string performanceBody1 = "";
        string performanceBody2 = "";
        string performanceBody3 = "";
        string performanceBody4 = "";
        string performanceBody5 = "";
        string performanceSent1 = "";
        string performanceSent2 = "";
        string performanceSent3 = "";

        string performanceParentsSubject = "";
        string performanceParentsBody1 = "";
        string performanceParentsBody2 = "";
        string performanceParentsBody3 = "";
        string performanceParentsBody4 = "";
        string performanceParentsBody5 = "";
        string performanceParentsSent1 = "";
        string performanceParentsSent2 = "";
        string performanceParentsSent3 = "";

        GmailService usersGmailService; // Google профиль пользователя
        string usersEmail; // Почтовый адрес пользователя
        string username = "";

        List<int> studentsIDs = new List<int>();
        string group; // Название группы
        string subject;
        string activity; // Название активности
        string date; // Дата
        string students; // Колличество студентов

        public Form_Send_Marks(string filePath, GmailService gmailService, SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();
            // Считывание списка групп и списка студентов из файлов

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            username = settingsXML.Element("settings").Element("username").Value;
            localization = XDocument.Load("lang/" + language + ".xml");

            updateLocalization(); // Обновление локализации окна

            usersGmailService = gmailService; // Сохранение сервисов пользователя

            // Получение почтового адреса пользователя
            UsersResource.GetProfileRequest requestProfile = usersGmailService.Users.GetProfile("me");
            Profile profile = requestProfile.Execute();
            usersEmail = profile.EmailAddress;

            Excel.Application excelApp = new Excel.Application(); // Создание объекста приложения Excel
            excelApp.Workbooks.Open(filePath); // Открытие документа Excel

            string groupName = excelApp.Worksheets[1].Range["A2"].Value; // Считывание номера группы
            string subjectName = excelApp.Worksheets[1].Range["B2"].Value; // Считывание названия предмета
            bool groupFound = false;
            bool subjectFound = false;

            answerTable = new DataTable();
            try
            {
                // Поиск группы в базе данных
                sqlQuery = "SELECT groupName FROM groups";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        if (groupName.Equals(answerTable.Rows[i].ItemArray[0])) groupFound = true;
                    }
                }

                // Поиск названия предмета в базе данных
                sqlQuery = "SELECT subjectName FROM subjects";
                answerTable = new DataTable();
                adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                adapter.Fill(answerTable);

                if (answerTable.Rows.Count > 0)
                {
                    for (int i = 0; i < answerTable.Rows.Count; i++)
                    {
                        if (subjectName.Equals(answerTable.Rows[i].ItemArray[0])) subjectFound = true;
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            if ((groupFound)&&(subjectFound)) // Если группа найдена
            {
                // Считывает и выводит информацию о названии группы, названии активности, дате и кол-ве студентов
                group = excelApp.Worksheets[1].Range["A2"].Value;
                label_Group.Text = groupLocalization + ": " + group;
                subject = excelApp.Worksheets[1].Range["B2"].Value;
                label_Subject.Text = subjectLocalizaion + ": " + subject;
                activity = excelApp.Worksheets[1].Range["C2"].Value;
                label_Activity.Text = activityLocalization + ": " + activity;
                date = excelApp.Worksheets[1].Range["D2"].Value;
                label_Date.Text = dateLocalization + ": " + date;
                students = "" + excelApp.Worksheets[1].Range["E2"].Value;
                label_Students.Text = studentsLocalization + ": " + excelApp.Worksheets[1].Range["E2"].Value;

                // Считывание информации о студентах группы из базы данных
                answerTable = new DataTable();
                try
                {
                    sqlQuery = "SELECT studentsSurname || ' ' || studentsName || ' ' || studentsPatronym, studentsPhoneNumber, studentsEmail, groupName, " +
                               "parentsFirstSurname || ' ' || parentsFirstName || ' ' || parentsFirstPatronym, parentsFirstPhoneNumber, parentsFirstEmail, " +
                               "parentsSecondSurname || ' ' || parentsSecondName || ' ' || parentsSecondPatronym, parentsSecondPhoneNumber, parentsSecondEmail, studentsId " +
                               "FROM students WHERE groupName='" + groupName +  "'" +
                               "ORDER BY studentsSurname, studentsName, studentsPatronym";
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
                    adapter.Fill(answerTable);
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }

                int quantityOfStudents = Convert.ToInt32(students); // Колличество студентов

                // Считывание данных студентов из документа (имя и оценка)
                for (int i = 0; i < quantityOfStudents; i++)
                {
                    string name = excelApp.Worksheets[1].Range["A" + (i + 4)].Value;                
                    string mark = Convert.ToString(excelApp.Worksheets[1].Range["B" + (i + 4)].Value);
                    string email = "";
                    string parentsFirstName = "";
                    string email1 = "";
                    string parentsSecondName = "";
                    string email2 = "";
                    for(int j = 0; j < answerTable.Rows.Count; j++)
                    {
                        // Если ммя из списка и имя в документе совпадают
                        if (answerTable.Rows[i].ItemArray[0].ToString().Equals(name)) 
                        {
                            // Считывание почтовых адресов из списка
                            email = answerTable.Rows[i].ItemArray[2].ToString();
                            parentsFirstName = answerTable.Rows[i].ItemArray[4].ToString();
                            email1 = answerTable.Rows[i].ItemArray[6].ToString();
                            parentsSecondName = answerTable.Rows[i].ItemArray[7].ToString(); ;
                            email2 = answerTable.Rows[i].ItemArray[9].ToString(); ;

                            dataGridView1.Rows.Add("" + (i + 1), name, mark, email, parentsFirstName, email1, parentsSecondName, email2); // Добавление данных в таблицу
                            studentsIDs.Add(Convert.ToInt32(answerTable.Rows[i].ItemArray[10]));
                            break;
                        }
                    }
                    
                }


            }
            else // Если группа не была найдена
            {
                MessageBox.Show("Group/Subject not found or bad document");
                this.Close();
            }
            
            // Закрыть текущий файл и выйти из приложения
            excelApp.ActiveWorkbook.Close(false);
            excelApp.Quit();
        }

        public void updateLocalization() // Метод обновления локализации формы
        {
            this.Text = localization.Element("localization").Element("sendMarks").Element("sendMarks").Value;

            dataGridView1.Columns[0].HeaderText = localization.Element("localization").Element("sendMarks").Element("number").Value;
            dataGridView1.Columns[1].HeaderText = localization.Element("localization").Element("sendMarks").Element("name").Value;
            dataGridView1.Columns[2].HeaderText = localization.Element("localization").Element("sendMarks").Element("mark").Value;
            dataGridView1.Columns[3].HeaderText = localization.Element("localization").Element("sendMarks").Element("studentsEmail").Value;
            dataGridView1.Columns[4].HeaderText = localization.Element("localization").Element("sendMarks").Element("parentsName1").Value;
            dataGridView1.Columns[5].HeaderText = localization.Element("localization").Element("sendMarks").Element("parentsFirstEmail").Value;
            dataGridView1.Columns[6].HeaderText = localization.Element("localization").Element("sendMarks").Element("parentsName2").Value;
            dataGridView1.Columns[7].HeaderText = localization.Element("localization").Element("sendMarks").Element("parentsSecondEmail").Value;

            numberLocalization = localization.Element("localization").Element("sendMarks").Element("number").Value;
            nameLocalization = localization.Element("localization").Element("sendMarks").Element("name").Value;
            markLocalization = localization.Element("localization").Element("sendMarks").Element("mark").Value;

            groupBox_Information.Text = localization.Element("localization").Element("sendMarks").Element("information").Value;

            dateLocalization = localization.Element("localization").Element("sendMarks").Element("date").Value;
            groupLocalization = localization.Element("localization").Element("sendMarks").Element("group").Value;
            subjectLocalizaion = localization.Element("localization").Element("sendMarks").Element("subject").Value;
            activityLocalization = localization.Element("localization").Element("sendMarks").Element("activity").Value;
            studentsLocalization = localization.Element("localization").Element("sendMarks").Element("students").Value;

            groupBox_Parameters.Text = localization.Element("localization").Element("sendMarks").Element("parameters").Value;
            checkBoxUploadMarks.Text = localization.Element("localization").Element("sendMarks").Element("uploadMarks").Value;
            checkBoxSendReport.Text = localization.Element("localization").Element("sendMarks").Element("sendReport").Value;
            checkBoxSendToStudents.Text = localization.Element("localization").Element("sendMarks").Element("sendToStudents").Value;
            checkBoxSendToParentsFirstEmail.Text = localization.Element("localization").Element("sendMarks").Element("sendToParentsFirstEmail").Value;
            checkBoxSendToParentsSecondEmail.Text = localization.Element("localization").Element("sendMarks").Element("sendToParentsSecondEmail").Value;

            buttonSendMarks.Text = localization.Element("localization").Element("sendMarks").Element("sendMarks").Value;

            reportSubject = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("subject").Value;
            reportBody1 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body1").Value;
            reportBody2 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body2").Value;
            reportBody3 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body3").Value;
            reportBody4 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body4").Value;
            reportBody5 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body5").Value;
            reportBody6 = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("body6").Value;
            reportSent = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("reportSent").Value;
            reportError = localization.Element("localization").Element("sendMarks").Element("mail").Element("report").Element("reportSent").Value;

            performanceSubject = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("subject").Value;
            performanceBody1 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("body1").Value;
            performanceBody2 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("body2").Value;
            performanceBody3 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("body3").Value;
            performanceBody4 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("body4").Value;
            performanceBody5 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("body5").Value;
            performanceSent1 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("messagesSent1").Value;
            performanceSent2 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("messagesSent2").Value;
            performanceSent3 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performance").Element("messagesSent3").Value;

            performanceParentsSubject = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("subject").Value;
            performanceParentsBody1 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("body1").Value;
            performanceParentsBody2 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("body2").Value;
            performanceParentsBody3 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("body3").Value;
            performanceParentsBody4 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("body4").Value;
            performanceParentsBody5 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("body5").Value;
            performanceParentsSent1 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("messagesSent1").Value;
            performanceParentsSent2 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("messagesSent2").Value;
            performanceParentsSent3 = localization.Element("localization").Element("sendMarks").Element("mail").Element("performanceParents").Element("messagesSent3").Value;
        }

        public static string Base64UrlEncode(string input) // Кодирование строк в Base64Url
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
        }

        public bool sendEmail(string to, string subject, string body) // Функция, что выполняет компоновку электронного письма и его отправку
        {
            try
            {
                subject = "=?utf-8?B?" + Base64UrlEncode(subject) + "?="; // Кодировка темы письма

                // Компоновка MIME-письма
                string plainText = "From: " + usersEmail + "\r\n" +
                                   "To:  " + to + " \r\n" +
                                   "Subject: " + subject + "\r\n" +
                                   "MIME-Version: 1.0\r\n" + 
                                   "Content-Type: text/html; charset=utf-8\r\n\r\n" +
                                   body;

                Google.Apis.Gmail.v1.Data.Message newMessage = new Google.Apis.Gmail.v1.Data.Message(); // Создание нового письма
                newMessage.Raw = Base64UrlEncode(plainText); // Декодирование в Base64URL
                usersGmailService.Users.Messages.Send(newMessage, "me").Execute(); // Отправка письма
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void buttonSendMarks_Click(object sender, EventArgs e) // Нажатие на кнопку "Отправить оценки"
        {
            buttonSendMarks.Enabled = false; // Отключение кнопки

            if(checkBoxUploadMarks.Checked) // Если выбрано "Сохранить оценки"
            {
                bool alreadyAdded = false;
                try
                {
                    int quantity = Convert.ToInt32(students);
                    for (int i = 0; i < quantity; i++)
                    {
                        // Добавление оценок в базу данных
                        sqlCommand.CommandText = "INSERT INTO marks(studentsId, subjectName, activity, date, mark)" +
                                                 "VALUES(" + studentsIDs[i] + ", '" + subject + "', '" + activity + "', '" + DateTime.Parse(date).ToString("yyyy-MM-dd") + "', '" + dataGridView1.Rows[i].Cells[2].Value + "')";
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                catch(SQLiteException)
                {
                    alreadyAdded = true;
                }
                if(alreadyAdded) MessageBox.Show("One or more of this marks have been already added to database");
            }

            if(checkBoxSendReport.Checked) // Если выбрано "Отправить отчёт"
            {
                string subject = reportSubject + " \"" + activity + "\", " + date;
                string body = "";

                // Компоновка тела письма
                body += reportBody1 + " " + username + ",<br/>";
                body += reportBody2 + " \"" + activity + "\"<br/>";
                body += reportBody3 + " " + date + ". <br/>";
                body += reportBody4 + " " + group + " " + reportBody5 + ":<br/>";
                body += "<table border=\"1\" cellpadding=\"5\" style = \"border-collapse: collapse; border: 1px solid black;\">";
                body += "<tr>";
                body += "<th>" + numberLocalization + "</th>";
                body += "<th>" + nameLocalization + "</th>";
                body += "<th>" + markLocalization + "</th>";
                body += "</tr>";

                // Вывод в тело письма всех оценок
                int quantity = Convert.ToInt32(students); 
                for (int i = 0; i < quantity; i++)
                {
                    body += "<tr>";
                    body += "<td>" + Convert.ToString(i+1) + "</td>";
                    body += "<td>" + dataGridView1.Rows[i].Cells[1].Value + "</td>";
                    body += "<td>" + dataGridView1.Rows[i].Cells[2].Value + "</td>";
                    body += "</tr>";
                }

                body += "</table>";
                body += reportBody6 + ": " + students;

                // Отправка пислма и сохранение результата отправки
                bool result = sendEmail(usersEmail, subject, body);
                if(result)
                {
                    MessageBox.Show(reportSent);
                }
                else
                {  
                    MessageBox.Show(reportError);
                }
            }

            if(checkBoxSendToStudents.Checked) // Если выбрано отправить оценки студентам
            {
                int succeed = 0;
                int failed = 0;
                bool result;
                int quantity = Convert.ToInt32(students);
                string studentsName = "";
                string studentsEmail = "";
                string studentsMark = "";
                string subject = "";
                string body = "";

                for (int i = 0; i < quantity; i++)
                {
                    studentsName = dataGridView1.Rows[i].Cells[1].Value.ToString(); // Считывание имени студента
                    studentsMark = dataGridView1.Rows[i].Cells[2].Value.ToString(); // Считывание оценки студента
                    studentsEmail = dataGridView1.Rows[i].Cells[3].Value.ToString(); // Считывание почтового адреса студента
                    subject = performanceSubject + " \"" + activity + "\", " + date;

                    // Компоновка тела письма
                    body = "";
                    body += performanceBody1 + " " + studentsName + ",<br/>";
                    body += performanceBody2 + " " + group + " " + performanceBody3 + " " + activity + ".<br/>";
                    body += performanceBody4 + ": <br/>";
                    body += "<table border=\"1\" cellpadding=\"5\" style = \"border-collapse: collapse; border: 1px solid black;\">";

                    body += "<tr>";
                    body += "<th>" + numberLocalization + "</th>";
                    body += "<th>" + nameLocalization + "</th>";
                    body += "<th>" + markLocalization + "</th>";
                    body += "</tr>";
                    body += "<tr>";
                    body += "<td>" + Convert.ToString(i + 1) + "</td>";
                    body += "<td>" + studentsName + "</td>";
                    body += "<td>" + studentsMark + "</td>";
                    body += "</tr>";
                    body += "</table>";
                    body += performanceBody5;

                    // Отправка письма
                    result = sendEmail(studentsEmail, subject, body);
                    if (result) succeed++;
                    if (!result) failed++;
                }

                MessageBox.Show(performanceSent1 + "\n" + performanceSent2 + " " + succeed.ToString() + "\n" + performanceSent3 + ": " + failed.ToString()); // Вывод статистики отправки писем
            }

            if(checkBoxSendToParentsFirstEmail.Checked) // Если выбрано отправить на первый почтовый адрес родителей
            {
                int succeed = 0;
                int failed = 0;
                bool result;
                int quantity = Convert.ToInt32(students);
                string studentsName = "";
                string parentsFirstName = "";
                string parentsFirstEmail = "";
                string studentsMark = "";
                string subject = "";
                string body = "";

                for (int i = 0; i < quantity; i++)
                {
                    // Считывание данных из таблицы
                    studentsName = dataGridView1.Rows[i].Cells[1].Value.ToString();
                    parentsFirstName = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    studentsMark = dataGridView1.Rows[i].Cells[2].Value.ToString();
                    parentsFirstEmail = dataGridView1.Rows[i].Cells[5].Value.ToString();
                    subject = performanceParentsSubject + " \"" + activity + "\", " + date;

                    // Компоновка тела письма
                    body = "";
                    body += performanceParentsBody1 + " " + parentsFirstName + ",<br/>";
                    body += performanceParentsBody2 + " " + group + " " + performanceParentsBody3 + " " + activity + ".<br/>";
                    body += performanceParentsBody4 + ": <br/>";
                    body += "<table border=\"1\" cellpadding=\"5\" style = \"border-collapse: collapse; border: 1px solid black;\">";

                    body += "<tr>";
                    body += "<th>" + numberLocalization + "</th>";
                    body += "<th>" + nameLocalization + "</th>";
                    body += "<th>" + markLocalization + "</th>";
                    body += "</tr>";
                    body += "<tr>";
                    body += "<td>" + Convert.ToString(i + 1) + "</td>";
                    body += "<td>" + studentsName + "</td>";
                    body += "<td>" + studentsMark + "</td>";
                    body += "</tr>";
                    body += "</table>";
                    body += performanceParentsBody5;

                    // Отправка письма
                    result = sendEmail(parentsFirstEmail, subject, body);
                    if (result) succeed++;
                    if (!result) failed++;
                }

                MessageBox.Show(performanceSent1 + "\n" + performanceSent2 + " " + succeed.ToString() + "\n" + performanceSent3 + ": " + failed.ToString()); // Вывод статистики отправки писем
            }

            if (checkBoxSendToParentsSecondEmail.Checked) // Если выбрано отправить на второй почтовый адрес родителей
            {
                int succeed = 0;
                int failed = 0;
                bool result;
                int quantity = Convert.ToInt32(students);
                string studentsName = "";
                string parentsSecondName = "";
                string parentsSecondEmail = "";
                string studentsMark = "";
                string subject = "";
                string body = "";

                for (int i = 0; i < quantity; i++)
                {
                    // Считывание данных из таблицы
                    studentsName = dataGridView1.Rows[i].Cells[1].Value.ToString();
                    parentsSecondName = dataGridView1.Rows[i].Cells[6].Value.ToString();
                    studentsMark = dataGridView1.Rows[i].Cells[2].Value.ToString();
                    parentsSecondEmail = dataGridView1.Rows[i].Cells[7].Value.ToString();

                    if ((parentsSecondName.Equals(""))||(parentsSecondEmail.Equals(""))) continue; // Если почтовый адрес не указан - пропустить

                    subject = performanceParentsSubject + " \"" + activity + "\", " + date;

                    // Компоновка тела письма
                    body = "";
                    body += performanceParentsBody1 + " " + parentsSecondName + ",<br/>";
                    body += performanceParentsBody2 + " " + group + " " + performanceParentsBody3 + " " + activity + ".<br/>";
                    body += performanceParentsBody4 + ": <br/>";
                    body += "<table border=\"1\" cellpadding=\"5\" style = \"border-collapse: collapse; border: 1px solid black;\">";

                    body += "<tr>";
                    body += "<th>" + numberLocalization + "</th>";
                    body += "<th>" + nameLocalization + "</th>";
                    body += "<th>" + markLocalization + "</th>";
                    body += "</tr>";
                    body += "<tr>";
                    body += "<td>" + Convert.ToString(i + 1) + "</td>";
                    body += "<td>" + studentsName + "</td>";
                    body += "<td>" + studentsMark + "</td>";
                    body += "</tr>";
                    body += "</table>";
                    body += performanceParentsBody5;

                    result = sendEmail(parentsSecondEmail, subject, body); // Отправка письма
                    if (result) succeed++;
                    if (!result) failed++;
                }

                MessageBox.Show(performanceSent1 + "\n" + performanceSent2 + " " + succeed.ToString() + "\n" + performanceSent3 + ": " + failed.ToString()); // Вывод статистики отправки писем
            }
            buttonSendMarks.Enabled = true;
        }

        private void checkBoxIsNecessaryToSave(object sender, EventArgs e) // Проверка обязательности сохранения оценок
        {
            if((checkBoxSendReport.Checked)||(checkBoxSendToParentsFirstEmail.Checked)||(checkBoxSendToParentsSecondEmail.Checked)||(checkBoxSendToStudents.Checked)) // Если выбрано хотя бы одно поле
            {
                // Сохранение обязательно
                checkBoxUploadMarks.Checked = true;
                checkBoxUploadMarks.Enabled = false;
            }
            else
            {
                checkBoxUploadMarks.Enabled = true;
            }
        }
    }
}