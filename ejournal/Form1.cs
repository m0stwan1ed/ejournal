﻿using System;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

// Библиотеки для работы с GMail API
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;

// Библитека для работы с потоками
using System.Threading;

// Библиотека для работы с XML
using System.Xml.Linq;

using System.Net.NetworkInformation;
using System.Net;
using System.Diagnostics;
using Ionic.Zip;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_eJournal_mainWindow : Form
    {
        SQLiteConnection databaseConnection = new SQLiteConnection(); // Соединение с базой данных
        SQLiteCommand sqlCommand = new SQLiteCommand(); // Запрос к базе данных

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML
        string noAccount = "";

        string databaseFilename = "ejournal1.sqlite3"; // Название файла базы данных
        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\"; // Адрес хранения файлов приложения
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml"; // Адрес файла настроек

        
        // Токен доступа к данным Гугл
        string clientId;
        // Пароль доступа
        string clientSecret;
        // Версия приложения
        string version; 

        UserCredential credential; // Данные пользователя
        GmailService gmailService; // Сервисы Гугл почты

        public Form_eJournal_mainWindow() // Конструктор формы
        {
            InitializeComponent();

            GoogleAPIKey apiKey = new GoogleAPIKey();
            clientId = apiKey.getClientId();
            clientSecret = apiKey.getClientSecret();

            // Если каталог не существует - создать каталог
            if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal"))
            {
                Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\");
            }

            // Если файл настроек не существует - создать файл настроек
            if (!File.Exists(settingsXMLPath))
            {
                settingsXML = new XDocument(
                        new XElement("settings",
                            new XElement("version", "1.3.1 Alpha"),
                            new XElement("language", "Українська"),
                            new XElement("username", "Anonymous")
                        )
                    );
                settingsXML.Save(settingsXMLPath);
            }

            // Если файл базы данных не существует - создать файл базы данных
            if (!File.Exists(folderPath + databaseFilename))
            {
                SQLiteConnection.CreateFile(folderPath + databaseFilename);
            }

            // Попытка подключения к базе данных и создания структуры таблиц, если они отсутствуют
            try
            {
                databaseConnection = new SQLiteConnection("Data Source=" + folderPath + databaseFilename + ";Version=3;foreign keys=true;");
                databaseConnection.Open();
                sqlCommand.Connection = databaseConnection;
                sqlCommand.CommandText = "CREATE TABLE IF NOT EXISTS groups (" +
                                         "groupName TEXT PRIMARY KEY" +
                                         ")";
                sqlCommand.ExecuteNonQuery();

                sqlCommand.CommandText = "CREATE TABLE IF NOT EXISTS students (studentsId INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                         "studentsSurname TEXT NOT NULL, " +
                                         "studentsName TEXT NOT NULL, " +
                                         "studentsPatronym TEXT NOT NULL, " +
                                         "studentsPhoneNumber TEXT, " +
                                         "studentsEmail TEXT NOT NULL, " +
                                         "groupName TEXT NOT NULL, " +
                                         "parentsFirstName TEXT, " +
                                         "parentsFirstSurname TEXT, " +
                                         "parentsFirstPatronym TEXT, " +
                                         "parentsFirstPhoneNumber TEXT, " +
                                         "parentsFirstEmail TEXT, " +
                                         "parentsSecondName TEXT, " +
                                         "parentsSecondSurname TEXT, " +
                                         "parentsSecondPatronym TEXT, " +
                                         "parentsSecondPhoneNumber TEXT, " +
                                         "parentsSecondEmail TEXT, " +
                                         "FOREIGN KEY(groupName) REFERENCES groups(groupName)" +
                                         ")";

                sqlCommand.ExecuteNonQuery();

                sqlCommand.CommandText = "CREATE TABLE IF NOT EXISTS subjects (" +
                         "subjectName TEXT PRIMARY KEY" +
                         ")";
                sqlCommand.ExecuteNonQuery();

                sqlCommand.CommandText = "CREATE TABLE IF NOT EXISTS marks (" +
                                         "studentsId INTEGER NOT NULL, " +
                                         "subjectName TEXT NOT NULL, " +
                                         "activity TEXT NOT NULL, " +
                                         "date TEXT NOT NULL," +
                                         "mark INTEGER, " +
                                         "CONSTRAINT marksPrimaryKey PRIMARY KEY (studentsId, subjectName, activity, date)" +
                                         "FOREIGN KEY(studentsId) REFERENCES students(studentsId), " +
                                         "FOREIGN KEY(subjectName) REFERENCES subjects(subjectName)" +
                                         ");";
                sqlCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            settingsXML = XDocument.Load(settingsXMLPath);
            string language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization(); // Обновление локализации

            version = settingsXML.Element("settings").Element("version").Value; // Считывание версии приложения
            label_version.Text = localization.Element("localization").Element("main").Element("version").Value + ": " + version; // Вывод версии на экран

            label_login_status.Text = noAccount; // По умолчанию при запуске авторизации нет

            // Запуск потока проверки наличия новой версии приложения
            System.Threading.Thread checkUpdatesThread = new System.Threading.Thread(checkUpdates)
            {
                IsBackground = true
            };
            checkUpdatesThread.Start();
        }

        public void updateLocalization() // Метод обновления локализации окна
        {
            this.Text = localization.Element("localization").Element("main").Element("main").Value;
            noAccount = localization.Element("localization").Element("main").Element("noAccount").Value;
            label_username.Text = settingsXML.Element("settings").Element("username").Value;
            button_login.Text = localization.Element("localization").Element("main").Element("login").Value;
            button_logout.Text = localization.Element("localization").Element("main").Element("logout").Value;
            button_send_marks.Text = localization.Element("localization").Element("main").Element("sendMarks").Value;
            button_reports.Text = localization.Element("localization").Element("main").Element("reports").Value;
            button_generate_excel.Text = localization.Element("localization").Element("main").Element("generateExcel").Value;
            button_manage_data.Text = localization.Element("localization").Element("main").Element("manageData").Value;
            button_Settings.Text = localization.Element("localization").Element("main").Element("settings").Value;
            linkLabel_Update.Text = localization.Element("localization").Element("main").Element("installUpdate").Value;
        }

        public void checkUpdates() // Метод проверки обновлений
        {
            string checking = localization.Element("localization").Element("main").Element("checking").Value + "...";
            string available = localization.Element("localization").Element("main").Element("serviceAvailable").Value;
            string unaavailable = localization.Element("localization").Element("main").Element("serviceUnavailable").Value;
            string noUpdate = localization.Element("localization").Element("main").Element("noUpdate").Value;
            string updateAvailable = localization.Element("localization").Element("main").Element("updateAvailable").Value;

            try
            {
                // Если файл обновления доступен выполнить распаковку обновлений
                if (File.Exists("update.zip")) 
                {
                    if (label_service_status.InvokeRequired)
                    {
                        label_service_status.Invoke(new Action(delegate () { label_service_status.Text = "Updating launcher..."; }));
                    }
                    else label_service_status.Text = "Updating launcher...";

                    using (ZipFile zip1 = ZipFile.Read("update.zip"))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            e.Extract(System.IO.Path.GetDirectoryName(Application.ExecutablePath), ExtractExistingFileAction.OverwriteSilently);
                        }
                    }
                    File.Delete("update.zip");

                    if (label_service_status.InvokeRequired)
                    {
                        label_service_status.Invoke(new Action(delegate () { label_service_status.Text = "Launcher updated"; }));
                    }
                    else label_service_status.Text = "Launcher updated";
                }

                if (label_service_status.InvokeRequired)
                {
                    label_service_status.Invoke(new Action(delegate () { label_service_status.Text = checking; }));
                }
                else label_service_status.Text = checking; 

                // Проверка доступности сервера обновлений
                Ping ping = new Ping();
                PingReply p = ping.Send("ejournal.ga");

                // Если сервер доступен - проверить наличие обновлений
                if (p.Status.ToString().Equals("Success"))
                {
                    WebRequest requestUpdateData = WebRequest.Create("http://ejournal.ga/updateService/update.xml");
                    WebResponse responseUpdateData = requestUpdateData.GetResponse();
                    Stream answerStream = responseUpdateData.GetResponseStream();
                    StreamReader srAnswer = new StreamReader(answerStream);
                    string updateData = srAnswer.ReadToEnd();

                    XDocument updateXML = XDocument.Parse(updateData);
                    string availableVersion = updateXML.Element("update").Element("version").Value;

                    if (version.Equals(availableVersion))
                    {
                        label_service_status.Invoke(new Action(delegate () { label_service_status.Text = noUpdate; }));
                    }
                    else
                    {
                        label_service_status.Invoke(new Action(delegate () { label_service_status.Text = updateAvailable; }));
                        linkLabel_Update.Invoke(new Action(delegate () { linkLabel_Update.Visible = true; }));
                    }
                }
                else
                {
                    label_service_status.Invoke(new Action(delegate () { label_service_status.Text = unaavailable; }));
                }
            }
            catch(Exception e)
            {
                if (e is PingException)
                {
                    label_service_status.Invoke(new Action(delegate () { label_service_status.Text = unaavailable; }));
                }
                if (e is WebException)
                {
                    label_service_status.Invoke(new Action(delegate () { label_service_status.Text = noUpdate; }));
                }
            }
        }

        private void button_login_Click(object sender, EventArgs e) // Нажатие на кнопку "Авторизироваться"
        {
            // Асинхронная авторизация в браузере и получение данных пользователя
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync
            (
                new ClientSecrets
                {
                    ClientId = clientId,
                    ClientSecret = clientSecret
                },
                new[] { "https://mail.google.com/ email" },
                "User",
                CancellationToken.None,
                new FileDataStore("Analytics.Auth.Store")
            ).Result;

            // Объявление Гугл сервисов
            gmailService = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "ApiGmailPlayground"

            });

            // Получаем логин (емейл) аккаунта Гугл
            UsersResource.GetProfileRequest requestProfile = gmailService.Users.GetProfile("me");
            Profile profile = requestProfile.Execute();
            label_login_status.Text = profile.EmailAddress; // Выводим логин на экран

            // После авторизации кнопки активируются
            button_logout.Enabled = true;
            button_login.Enabled = false;
            button_manage_data.Enabled = true;
            button_generate_excel.Enabled = true;
            button_send_marks.Enabled = true;
            button_reports.Enabled = true;
        }

        private void button_logout_Click(object sender, EventArgs e) // Нажатие на кнопку "Деавторизация"
        {
            credential.RevokeTokenAsync(CancellationToken.None); // Деактивация токена доступа

            // Вывод на экран сообщения о деавторизации и деактивация кнопок
            label_login_status.Text = noAccount;
            button_logout.Enabled = false;
            button_login.Enabled = true;
            button_manage_data.Enabled = false;
            button_generate_excel.Enabled = false;
            button_send_marks.Enabled = false;
            button_reports.Enabled = false;
        }

        private void button_manage_students_Click(object sender, EventArgs e) // Нажатие на кнопку "Управление студентами"
        {
            Form formMangeData = new Form_Manage_Data(databaseConnection, sqlCommand); // Инициализация окна
            formMangeData.ShowDialog(); // Отображение окна в виде диалогового
            formMangeData.Dispose(); // Уничтожение окна после его закрытия
        }

        private void button_generate_excel_Click(object sender, EventArgs e) // Нажатие на кнопку "Генерировать документ"
        {
            Form formGenerateExcel = new Form_Generate_Excel(databaseConnection, sqlCommand); // Инициализация окна
            formGenerateExcel.ShowDialog(); // Отображение окна в виде диалогового
            formGenerateExcel.Dispose(); // Уничтожение окна после его закрытия
        }

        private void button_send_marks_Click(object sender, EventArgs e) // Нажатие на кнопку "Отправить оценки"
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) // Диалоговое окно открытия файла и проверка, был ли файл открыт
            {
                string filePath = openFileDialog1.FileName; // Получение пути к файлу
                try
                {
                    Form formSendMarks = new Form_Send_Marks(filePath, gmailService, databaseConnection, sqlCommand); // Инициализация окна и передача в конструктор окна пути к файлу и объекта сервисов Google 
                    formSendMarks.ShowDialog(); // Отображение окна в виде диалогового
                    formSendMarks.Dispose(); // Уничтожение окна после его закрытия 
                }
                catch(ObjectDisposedException)
                {

                }
            }
        }

        private void button_Settings_Click(object sender, EventArgs e) // Нажатие на кнопку "Настройки"
        {
            Form_Settings formSettings = new Form_Settings();
            formSettings.ShowDialog();
            formSettings.Dispose();
            updateLocalization();
        }

        private void linkLabel_Update_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) // Нажатие на ссылку "Установить обновление"
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "update.exe";
                Process.Start(startInfo);
                Application.Exit();
            }
            catch(Win32Exception)
            {
                string message = localization.Element("localization").Element("main").Element("installationError").Value;
                MessageBox.Show(message);
            }
            
        }

        private void button_Reports_Click(object sender, EventArgs e) // Нажатие на кнопку "Отчёты"
        {
            Form_Reports formReports = new Form_Reports(databaseConnection, sqlCommand);
            formReports.ShowDialog();
            formReports.Dispose();
        }
    }
}