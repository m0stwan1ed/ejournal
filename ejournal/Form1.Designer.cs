﻿namespace ejournal
{
    partial class Form_eJournal_mainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_eJournal_mainWindow));
            this.button_login = new System.Windows.Forms.Button();
            this.button_logout = new System.Windows.Forms.Button();
            this.label_login_status = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label_username = new System.Windows.Forms.Label();
            this.label_version = new System.Windows.Forms.Label();
            this.label_service_status = new System.Windows.Forms.Label();
            this.linkLabel_Update = new System.Windows.Forms.LinkLabel();
            this.button_reports = new System.Windows.Forms.Button();
            this.button_Settings = new System.Windows.Forms.Button();
            this.button_send_marks = new System.Windows.Forms.Button();
            this.button_generate_excel = new System.Windows.Forms.Button();
            this.button_manage_data = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_login
            // 
            this.button_login.Location = new System.Drawing.Point(12, 38);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(75, 23);
            this.button_login.TabIndex = 1;
            this.button_login.Text = "Log-in";
            this.button_login.UseVisualStyleBackColor = true;
            this.button_login.Click += new System.EventHandler(this.button_login_Click);
            // 
            // button_logout
            // 
            this.button_logout.Enabled = false;
            this.button_logout.Location = new System.Drawing.Point(93, 38);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(75, 23);
            this.button_logout.TabIndex = 2;
            this.button_logout.Text = "Log out";
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.Click += new System.EventHandler(this.button_logout_Click);
            // 
            // label_login_status
            // 
            this.label_login_status.AutoSize = true;
            this.label_login_status.Location = new System.Drawing.Point(12, 22);
            this.label_login_status.Name = "label_login_status";
            this.label_login_status.Size = new System.Drawing.Size(63, 13);
            this.label_login_status.TabIndex = 3;
            this.label_login_status.Text = "login_status";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 2007 and newer files(*.xlsx)|*.xlsx|Excel 2003 and older files(*.xls)|*.xls" +
    "";
            // 
            // label_username
            // 
            this.label_username.AutoSize = true;
            this.label_username.Location = new System.Drawing.Point(12, 9);
            this.label_username.Name = "label_username";
            this.label_username.Size = new System.Drawing.Size(81, 13);
            this.label_username.TabIndex = 8;
            this.label_username.Text = "label_username";
            // 
            // label_version
            // 
            this.label_version.Location = new System.Drawing.Point(425, 9);
            this.label_version.Name = "label_version";
            this.label_version.Size = new System.Drawing.Size(156, 13);
            this.label_version.TabIndex = 9;
            this.label_version.Text = "label_version";
            this.label_version.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_service_status
            // 
            this.label_service_status.Location = new System.Drawing.Point(425, 22);
            this.label_service_status.Name = "label_service_status";
            this.label_service_status.Size = new System.Drawing.Size(156, 13);
            this.label_service_status.TabIndex = 10;
            this.label_service_status.Text = "label_service_status";
            this.label_service_status.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // linkLabel_Update
            // 
            this.linkLabel_Update.Location = new System.Drawing.Point(425, 35);
            this.linkLabel_Update.Name = "linkLabel_Update";
            this.linkLabel_Update.Size = new System.Drawing.Size(156, 13);
            this.linkLabel_Update.TabIndex = 11;
            this.linkLabel_Update.TabStop = true;
            this.linkLabel_Update.Text = "linkLabel_Update";
            this.linkLabel_Update.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.linkLabel_Update.Visible = false;
            this.linkLabel_Update.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_Update_LinkClicked);
            // 
            // button_reports
            // 
            this.button_reports.Enabled = false;
            this.button_reports.Image = global::ejournal.Properties.Resources.ejournal_report;
            this.button_reports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_reports.Location = new System.Drawing.Point(12, 182);
            this.button_reports.Name = "button_reports";
            this.button_reports.Size = new System.Drawing.Size(145, 55);
            this.button_reports.TabIndex = 12;
            this.button_reports.Text = "Reports";
            this.button_reports.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_reports.UseVisualStyleBackColor = true;
            this.button_reports.Click += new System.EventHandler(this.button_Reports_Click);
            // 
            // button_Settings
            // 
            this.button_Settings.Image = global::ejournal.Properties.Resources.ejournal_settings;
            this.button_Settings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_Settings.Location = new System.Drawing.Point(436, 304);
            this.button_Settings.Name = "button_Settings";
            this.button_Settings.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_Settings.Size = new System.Drawing.Size(145, 55);
            this.button_Settings.TabIndex = 7;
            this.button_Settings.Text = "Settings";
            this.button_Settings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Settings.UseVisualStyleBackColor = true;
            this.button_Settings.Click += new System.EventHandler(this.button_Settings_Click);
            // 
            // button_send_marks
            // 
            this.button_send_marks.Enabled = false;
            this.button_send_marks.Image = global::ejournal.Properties.Resources.ejournal_email;
            this.button_send_marks.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_send_marks.Location = new System.Drawing.Point(12, 121);
            this.button_send_marks.Name = "button_send_marks";
            this.button_send_marks.Size = new System.Drawing.Size(145, 55);
            this.button_send_marks.TabIndex = 6;
            this.button_send_marks.Text = "Send marks";
            this.button_send_marks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_send_marks.UseVisualStyleBackColor = true;
            this.button_send_marks.Click += new System.EventHandler(this.button_send_marks_Click);
            // 
            // button_generate_excel
            // 
            this.button_generate_excel.Enabled = false;
            this.button_generate_excel.Image = global::ejournal.Properties.Resources.ejournal_excel;
            this.button_generate_excel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_generate_excel.Location = new System.Drawing.Point(12, 243);
            this.button_generate_excel.Name = "button_generate_excel";
            this.button_generate_excel.Size = new System.Drawing.Size(145, 55);
            this.button_generate_excel.TabIndex = 5;
            this.button_generate_excel.Text = "Generate Excel document";
            this.button_generate_excel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_generate_excel.UseVisualStyleBackColor = true;
            this.button_generate_excel.Click += new System.EventHandler(this.button_generate_excel_Click);
            // 
            // button_manage_data
            // 
            this.button_manage_data.Enabled = false;
            this.button_manage_data.Image = global::ejournal.Properties.Resources.ejournal_database;
            this.button_manage_data.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_manage_data.Location = new System.Drawing.Point(12, 304);
            this.button_manage_data.Name = "button_manage_data";
            this.button_manage_data.Size = new System.Drawing.Size(145, 55);
            this.button_manage_data.TabIndex = 4;
            this.button_manage_data.Text = "Manage data";
            this.button_manage_data.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_manage_data.UseVisualStyleBackColor = true;
            this.button_manage_data.Click += new System.EventHandler(this.button_manage_students_Click);
            // 
            // Form_eJournal_mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 371);
            this.Controls.Add(this.button_reports);
            this.Controls.Add(this.linkLabel_Update);
            this.Controls.Add(this.label_service_status);
            this.Controls.Add(this.label_version);
            this.Controls.Add(this.label_username);
            this.Controls.Add(this.button_Settings);
            this.Controls.Add(this.button_send_marks);
            this.Controls.Add(this.button_generate_excel);
            this.Controls.Add(this.button_manage_data);
            this.Controls.Add(this.label_login_status);
            this.Controls.Add(this.button_logout);
            this.Controls.Add(this.button_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_eJournal_mainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EJournal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_login;
        private System.Windows.Forms.Button button_logout;
        private System.Windows.Forms.Label label_login_status;
        private System.Windows.Forms.Button button_manage_data;
        private System.Windows.Forms.Button button_generate_excel;
        private System.Windows.Forms.Button button_send_marks;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button_Settings;
        private System.Windows.Forms.Label label_username;
        private System.Windows.Forms.Label label_version;
        private System.Windows.Forms.Label label_service_status;
        private System.Windows.Forms.LinkLabel linkLabel_Update;
        private System.Windows.Forms.Button button_reports;
    }
}

