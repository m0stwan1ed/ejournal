﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Библиотека для работы с XML
using System.Xml.Linq;

// Библиотека для работы с SQLite
using System.Data.SQLite;

namespace ejournal
{
    public partial class Form_List_Marks_Report : Form
    {
        SQLiteConnection databaseConnection; // Подключение к базе данных
        SQLiteCommand sqlCommand; // Запрос к базе данных

        string folderPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\";
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml";
        string language = "";

        string studentsName = "";
        string group = "";
        string subjectName = "";
        string activity = "";
        string date = "";
        string mark = "";

        XDocument settingsXML = new XDocument(); // Файл настроек XML
        XDocument localization = new XDocument(); // Объект для хранения файла локализации XML

        int quantityOfParameters;

        public Form_List_Marks_Report(SQLiteConnection databaseConnection, SQLiteCommand sqlCommand) // Конструктор формы
        {
            InitializeComponent();

            this.databaseConnection = databaseConnection;
            this.sqlCommand = sqlCommand;

            settingsXML = XDocument.Load(settingsXMLPath);
            language = settingsXML.Element("settings").Element("language").Value;
            localization = XDocument.Load("lang/" + language + ".xml");
            updateLocalization();

            quantityOfParameters = 0; // Колличество выбраных параметров в запроке к базе данных

            // Получение списка групп из базы данных
            string sqlQuery = "SELECT groupName FROM groups ORDER BY groupName";
            DataTable answerTable = new DataTable();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);

            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_group.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }

            // Получение списка предметов из базы данных
            sqlQuery = "SELECT subjectName FROM subjects ORDER BY subjectName";
            answerTable = new DataTable();
            adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_subject.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }
        }

        void updateLocalization()
        {
            this.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("listMarksReport").Value;
            groupBox1.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("parameters").Value;
            checkBox_group.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("group").Value;
            comboBox_group.Text = "[" + localization.Element("localization").Element("reports").Element("listMarksReport").Element("chooseGroup").Value + "]";
            checkBox_student.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("student").Value;
            comboBox_student.Text = "[" + localization.Element("localization").Element("reports").Element("listMarksReport").Element("chooseStudent").Value + "]";

            checkBox_subject.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("subject").Value;
            comboBox_subject.Text = "[" + localization.Element("localization").Element("reports").Element("listMarksReport").Element("chooseSubject").Value + "]";
            checkBox_activity.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("activity").Value;
            comboBox_activity.Text = "[" + localization.Element("localization").Element("reports").Element("listMarksReport").Element("chooseActivity").Value + "]";

            checkBox_date.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("date").Value;
            label_date_from.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("from").Value;
            label_date_to.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("to").Value;

            button_get_report.Text = localization.Element("localization").Element("reports").Element("listMarksReport").Element("getReport").Value;

            studentsName = localization.Element("localization").Element("reports").Element("listMarksReport").Element("studentsName").Value;
            group = localization.Element("localization").Element("reports").Element("listMarksReport").Element("group").Value;
            subjectName = localization.Element("localization").Element("reports").Element("listMarksReport").Element("subjectName").Value;
            activity = localization.Element("localization").Element("reports").Element("listMarksReport").Element("activity").Value;
            date = localization.Element("localization").Element("reports").Element("listMarksReport").Element("date").Value;
            mark = localization.Element("localization").Element("reports").Element("listMarksReport").Element("mark").Value; 
        } // Метод обновления локализации формы

        private void button_get_report_Click(object sender, EventArgs e) // Нажатие на кнопку "Получить отчёт"
        {
            DataTable answerTable = new DataTable(); // ответ базы данных в виде таблицы

            string dateFrom = dateTimePicker_from.Value.Date.ToString("yyyy-MM-dd");
            string dateTo= dateTimePicker_to.Value.Date.ToString("yyyy-MM-dd");

            string sqlQuery = "SELECT (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym) AS '" + studentsName + "', groupName AS '" + group + "', subjectName AS '" + subjectName + "', " +
                              "activity AS '" + activity + "', date AS '" + date + "', mark AS '" + mark + "'" +
                              "FROM students LEFT OUTER JOIN marks USING(studentsId) ";
            Boolean first = true;
            if ((checkBox_group.Checked) && (comboBox_group.SelectedItem != null)) // Если выбрано "Группа" ищём по группе
            {
                if ((quantityOfParameters == 1) || (first))
                {
                    sqlQuery += "WHERE groupName = '" + comboBox_group.SelectedItem.ToString() + "'";
                    first = false;
                }
                else
                {
                    sqlQuery += "AND groupName = '" + comboBox_group.SelectedItem.ToString() + "' ";
                }
            }

            if ((checkBox_student.Checked)&&(comboBox_student.SelectedItem != null)) // Если выбрано "Студент" ищем по студенту
            {
                if ((quantityOfParameters == 1) || (first))
                {
                    sqlQuery += "WHERE (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym) = '" + comboBox_student.SelectedItem.ToString() + "'";
                    first = false;
                }
                else
                {
                    sqlQuery += "AND (studentsSurname || ' ' || studentsName || ' ' || studentsPatronym) = '" + comboBox_student.SelectedItem.ToString() + "' ";
                }
            }

            if ((checkBox_subject.Checked) && (comboBox_subject.SelectedItem != null)) // Если выбрано "Предмет" ищем по предмету
            {
                if ((quantityOfParameters == 1) || (first))
                {
                    sqlQuery += "WHERE subjectName = '" + comboBox_subject.SelectedItem.ToString() + "'";
                    first = false;
                }
                else
                {
                    sqlQuery += "AND subjectName = '" + comboBox_subject.SelectedItem.ToString() + "' ";
                }
            }

            if ((checkBox_activity.Checked) && (comboBox_activity.SelectedItem != null)) // Если выбрано "Активность" ищем по активности
            {
                if ((quantityOfParameters == 1) || (first))
                {
                    sqlQuery += "WHERE activity = '" + comboBox_activity.SelectedItem.ToString() + "'";
                    first = false;
                }
                else
                {
                    sqlQuery += "AND activity = '" + comboBox_activity.SelectedItem.ToString() + "' ";
                }
            }

            if (checkBox_date.Checked) // Если выбрано "Дата" ижем по дате
            {
                if ((quantityOfParameters == 1) || (first))
                {
                    sqlQuery += "WHERE date BETWEEN '" + dateFrom + "' AND '" + dateTo + "'";
                    first = false;
                }
                else
                {
                    sqlQuery += "AND date BETWEEN '" + dateFrom + "' AND '" + dateTo + "' ";
                }
            }



            sqlQuery += "ORDER BY\"" + studentsName + "\""; // Сортируем по имени студента
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);

            dataGridView1.DataSource = answerTable; // Вывод ответа в форме
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells); // Настройка ширины столбцов
        }

        // Изменение состояний чекБоксов и колличества отмеченных полей
        private void checkBox_date_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_date.Checked)
            {
                label_date_from.Enabled = true;
                label_date_to.Enabled = true;
                dateTimePicker_from.Enabled = true;
                dateTimePicker_to.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                label_date_from.Enabled = false;
                label_date_to.Enabled = false;
                dateTimePicker_from.Enabled = false;
                dateTimePicker_to.Enabled = false;
                quantityOfParameters--;
            }
        }

        private void checkBox_group_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_group.Checked)
            {
                comboBox_group.Enabled = true;
                checkBox_student.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                comboBox_group.Enabled = false;
                checkBox_student.Enabled = false;
                checkBox_student.Checked = false;
                comboBox_student.Enabled = false;
                quantityOfParameters--;
            }
        }

        private void checkBox_student_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_student.Checked)
            {
                comboBox_student.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                comboBox_student.Enabled = false;
                comboBox_student.SelectedItem = null;
                quantityOfParameters--;
            }
        }

        private void checkBox_subject_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_subject.Checked)
            {
                comboBox_subject.Enabled = true;
                checkBox_activity.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                comboBox_subject.Enabled = false;
                checkBox_activity.Enabled = false;
                checkBox_activity.Checked = false;
                quantityOfParameters--;
            }
        }

        private void checkBox_activity_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox_activity.Checked)
            {
                comboBox_activity.Enabled = true;
                quantityOfParameters++;
            }
            else
            {
                comboBox_activity.Enabled = false;
                comboBox_activity.SelectedItem = null;
                quantityOfParameters--;
            }
        }
        //-------------------------------------------------------------

        private void comboBox_group_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlQuery = "SELECT (studentssurname || ' ' || studentsName || ' ' || studentsPatronym) AS studentsName " +
                              "FROM students " +
                              "WHERE groupName = '" + comboBox_group.SelectedItem.ToString() + "'" +
                              "ORDER BY studentsName";

            comboBox_student.Items.Clear();

            DataTable answerTable = new DataTable();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_student.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }
        } // Если выбрана група перезагрузить список учеников

        private void comboBox_subject_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sqlQuery = "SELECT activity " +
                              "FROM marks " +
                              "WHERE subjectName = '" + comboBox_subject.SelectedItem.ToString() + "' " +
                              "GROUP BY activity " +
                              "ORDER BY activity";

            comboBox_activity.Items.Clear();

            DataTable answerTable = new DataTable();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, databaseConnection);
            adapter.Fill(answerTable);
            if (answerTable.Rows.Count > 0)
            {
                for (int i = 0; i < answerTable.Rows.Count; i++)
                {
                    comboBox_activity.Items.Add(answerTable.Rows[i].ItemArray[0].ToString());
                }
            }
        } // Если выбран предмет перезагрузить список активностей
    }
}
