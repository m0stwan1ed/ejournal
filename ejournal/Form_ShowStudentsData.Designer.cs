﻿namespace ejournal
{
    partial class Form_ShowStudentsData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_studentsID_value = new System.Windows.Forms.Label();
            this.label_studentsID = new System.Windows.Forms.Label();
            this.label_students_surname = new System.Windows.Forms.Label();
            this.label_students_surname_value = new System.Windows.Forms.Label();
            this.label_students_name = new System.Windows.Forms.Label();
            this.label_students_patronym = new System.Windows.Forms.Label();
            this.label_students_name_value = new System.Windows.Forms.Label();
            this.label_students_patronym_value = new System.Windows.Forms.Label();
            this.label_students_phone_number = new System.Windows.Forms.Label();
            this.label_students_phone_number_value = new System.Windows.Forms.Label();
            this.label_students_email = new System.Windows.Forms.Label();
            this.label_students_email_value = new System.Windows.Forms.Label();
            this.label_students_group = new System.Windows.Forms.Label();
            this.label_students_group_value = new System.Windows.Forms.Label();
            this.label_parents_first_surname = new System.Windows.Forms.Label();
            this.label_parents_first_name = new System.Windows.Forms.Label();
            this.label_parents_first_patronym = new System.Windows.Forms.Label();
            this.label_parents_first_surname_value = new System.Windows.Forms.Label();
            this.label_parents_first_name_value = new System.Windows.Forms.Label();
            this.label_parents_first_patronym_value = new System.Windows.Forms.Label();
            this.label_parents_first_phone_number = new System.Windows.Forms.Label();
            this.label_parents_first_email = new System.Windows.Forms.Label();
            this.label_parents_first_phone_number_value = new System.Windows.Forms.Label();
            this.label_parents_first_email_value = new System.Windows.Forms.Label();
            this.label_parents_second_email_value = new System.Windows.Forms.Label();
            this.label_parents_second_phone_number_value = new System.Windows.Forms.Label();
            this.label_parents_second_email = new System.Windows.Forms.Label();
            this.label_parents_second_phone_number = new System.Windows.Forms.Label();
            this.label_parents_second_patronym_value = new System.Windows.Forms.Label();
            this.label_parents_second_name_value = new System.Windows.Forms.Label();
            this.label_parents_second_surname_value = new System.Windows.Forms.Label();
            this.label_parents_second_patronym = new System.Windows.Forms.Label();
            this.label_parents_second_name = new System.Windows.Forms.Label();
            this.label_parents_second_surname = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_edit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_studentsID_value
            // 
            this.label_studentsID_value.AutoSize = true;
            this.label_studentsID_value.Location = new System.Drawing.Point(165, 9);
            this.label_studentsID_value.Name = "label_studentsID_value";
            this.label_studentsID_value.Size = new System.Drawing.Size(118, 13);
            this.label_studentsID_value.TabIndex = 0;
            this.label_studentsID_value.Text = "label_studentsID_value";
            // 
            // label_studentsID
            // 
            this.label_studentsID.AutoSize = true;
            this.label_studentsID.Location = new System.Drawing.Point(18, 9);
            this.label_studentsID.Name = "label_studentsID";
            this.label_studentsID.Size = new System.Drawing.Size(71, 13);
            this.label_studentsID.TabIndex = 1;
            this.label_studentsID.Text = "Student\'s ID :";
            // 
            // label_students_surname
            // 
            this.label_students_surname.AutoSize = true;
            this.label_students_surname.Location = new System.Drawing.Point(6, 16);
            this.label_students_surname.Name = "label_students_surname";
            this.label_students_surname.Size = new System.Drawing.Size(55, 13);
            this.label_students_surname.TabIndex = 2;
            this.label_students_surname.Text = "Surname :";
            // 
            // label_students_surname_value
            // 
            this.label_students_surname_value.AutoSize = true;
            this.label_students_surname_value.Location = new System.Drawing.Point(153, 16);
            this.label_students_surname_value.Name = "label_students_surname_value";
            this.label_students_surname_value.Size = new System.Drawing.Size(153, 13);
            this.label_students_surname_value.TabIndex = 3;
            this.label_students_surname_value.Text = "label_students_surname_value";
            // 
            // label_students_name
            // 
            this.label_students_name.AutoSize = true;
            this.label_students_name.Location = new System.Drawing.Point(6, 29);
            this.label_students_name.Name = "label_students_name";
            this.label_students_name.Size = new System.Drawing.Size(41, 13);
            this.label_students_name.TabIndex = 4;
            this.label_students_name.Text = "Name :";
            // 
            // label_students_patronym
            // 
            this.label_students_patronym.AutoSize = true;
            this.label_students_patronym.Location = new System.Drawing.Point(6, 42);
            this.label_students_patronym.Name = "label_students_patronym";
            this.label_students_patronym.Size = new System.Drawing.Size(57, 13);
            this.label_students_patronym.TabIndex = 5;
            this.label_students_patronym.Text = "Patronym :";
            // 
            // label_students_name_value
            // 
            this.label_students_name_value.AutoSize = true;
            this.label_students_name_value.Location = new System.Drawing.Point(153, 29);
            this.label_students_name_value.Name = "label_students_name_value";
            this.label_students_name_value.Size = new System.Drawing.Size(139, 13);
            this.label_students_name_value.TabIndex = 6;
            this.label_students_name_value.Text = "label_students_name_value";
            // 
            // label_students_patronym_value
            // 
            this.label_students_patronym_value.AutoSize = true;
            this.label_students_patronym_value.Location = new System.Drawing.Point(153, 42);
            this.label_students_patronym_value.Name = "label_students_patronym_value";
            this.label_students_patronym_value.Size = new System.Drawing.Size(156, 13);
            this.label_students_patronym_value.TabIndex = 7;
            this.label_students_patronym_value.Text = "label_students_patronym_value";
            // 
            // label_students_phone_number
            // 
            this.label_students_phone_number.AutoSize = true;
            this.label_students_phone_number.Location = new System.Drawing.Point(6, 71);
            this.label_students_phone_number.Name = "label_students_phone_number";
            this.label_students_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_students_phone_number.TabIndex = 8;
            this.label_students_phone_number.Text = "Phone number :";
            // 
            // label_students_phone_number_value
            // 
            this.label_students_phone_number_value.AutoSize = true;
            this.label_students_phone_number_value.Location = new System.Drawing.Point(153, 71);
            this.label_students_phone_number_value.Name = "label_students_phone_number_value";
            this.label_students_phone_number_value.Size = new System.Drawing.Size(184, 13);
            this.label_students_phone_number_value.TabIndex = 9;
            this.label_students_phone_number_value.Text = "label_students_phone_number_value";
            // 
            // label_students_email
            // 
            this.label_students_email.AutoSize = true;
            this.label_students_email.Location = new System.Drawing.Point(6, 84);
            this.label_students_email.Name = "label_students_email";
            this.label_students_email.Size = new System.Drawing.Size(42, 13);
            this.label_students_email.TabIndex = 10;
            this.label_students_email.Text = "E-Mail :";
            // 
            // label_students_email_value
            // 
            this.label_students_email_value.AutoSize = true;
            this.label_students_email_value.Location = new System.Drawing.Point(153, 84);
            this.label_students_email_value.Name = "label_students_email_value";
            this.label_students_email_value.Size = new System.Drawing.Size(137, 13);
            this.label_students_email_value.TabIndex = 11;
            this.label_students_email_value.Text = "label_students_email_value";
            // 
            // label_students_group
            // 
            this.label_students_group.AutoSize = true;
            this.label_students_group.Location = new System.Drawing.Point(6, 97);
            this.label_students_group.Name = "label_students_group";
            this.label_students_group.Size = new System.Drawing.Size(42, 13);
            this.label_students_group.TabIndex = 12;
            this.label_students_group.Text = "Group :";
            // 
            // label_students_group_value
            // 
            this.label_students_group_value.AutoSize = true;
            this.label_students_group_value.Location = new System.Drawing.Point(153, 97);
            this.label_students_group_value.Name = "label_students_group_value";
            this.label_students_group_value.Size = new System.Drawing.Size(140, 13);
            this.label_students_group_value.TabIndex = 13;
            this.label_students_group_value.Text = "label_students_group_value";
            // 
            // label_parents_first_surname
            // 
            this.label_parents_first_surname.AutoSize = true;
            this.label_parents_first_surname.Location = new System.Drawing.Point(6, 16);
            this.label_parents_first_surname.Name = "label_parents_first_surname";
            this.label_parents_first_surname.Size = new System.Drawing.Size(55, 13);
            this.label_parents_first_surname.TabIndex = 15;
            this.label_parents_first_surname.Text = "Surname :";
            // 
            // label_parents_first_name
            // 
            this.label_parents_first_name.AutoSize = true;
            this.label_parents_first_name.Location = new System.Drawing.Point(6, 29);
            this.label_parents_first_name.Name = "label_parents_first_name";
            this.label_parents_first_name.Size = new System.Drawing.Size(41, 13);
            this.label_parents_first_name.TabIndex = 16;
            this.label_parents_first_name.Text = "Name :";
            // 
            // label_parents_first_patronym
            // 
            this.label_parents_first_patronym.AutoSize = true;
            this.label_parents_first_patronym.Location = new System.Drawing.Point(6, 42);
            this.label_parents_first_patronym.Name = "label_parents_first_patronym";
            this.label_parents_first_patronym.Size = new System.Drawing.Size(57, 13);
            this.label_parents_first_patronym.TabIndex = 17;
            this.label_parents_first_patronym.Text = "Patronym :";
            // 
            // label_parents_first_surname_value
            // 
            this.label_parents_first_surname_value.AutoSize = true;
            this.label_parents_first_surname_value.Location = new System.Drawing.Point(153, 16);
            this.label_parents_first_surname_value.Name = "label_parents_first_surname_value";
            this.label_parents_first_surname_value.Size = new System.Drawing.Size(170, 13);
            this.label_parents_first_surname_value.TabIndex = 18;
            this.label_parents_first_surname_value.Text = "label_parents_first_surname_value";
            // 
            // label_parents_first_name_value
            // 
            this.label_parents_first_name_value.AutoSize = true;
            this.label_parents_first_name_value.Location = new System.Drawing.Point(153, 29);
            this.label_parents_first_name_value.Name = "label_parents_first_name_value";
            this.label_parents_first_name_value.Size = new System.Drawing.Size(156, 13);
            this.label_parents_first_name_value.TabIndex = 19;
            this.label_parents_first_name_value.Text = "label_parents_first_name_value";
            // 
            // label_parents_first_patronym_value
            // 
            this.label_parents_first_patronym_value.AutoSize = true;
            this.label_parents_first_patronym_value.Location = new System.Drawing.Point(153, 42);
            this.label_parents_first_patronym_value.Name = "label_parents_first_patronym_value";
            this.label_parents_first_patronym_value.Size = new System.Drawing.Size(173, 13);
            this.label_parents_first_patronym_value.TabIndex = 20;
            this.label_parents_first_patronym_value.Text = "label_parents_first_patronym_value";
            // 
            // label_parents_first_phone_number
            // 
            this.label_parents_first_phone_number.AutoSize = true;
            this.label_parents_first_phone_number.Location = new System.Drawing.Point(6, 55);
            this.label_parents_first_phone_number.Name = "label_parents_first_phone_number";
            this.label_parents_first_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_parents_first_phone_number.TabIndex = 21;
            this.label_parents_first_phone_number.Text = "Phone number :";
            // 
            // label_parents_first_email
            // 
            this.label_parents_first_email.AutoSize = true;
            this.label_parents_first_email.Location = new System.Drawing.Point(6, 68);
            this.label_parents_first_email.Name = "label_parents_first_email";
            this.label_parents_first_email.Size = new System.Drawing.Size(42, 13);
            this.label_parents_first_email.TabIndex = 22;
            this.label_parents_first_email.Text = "E-Mail :";
            // 
            // label_parents_first_phone_number_value
            // 
            this.label_parents_first_phone_number_value.AutoSize = true;
            this.label_parents_first_phone_number_value.Location = new System.Drawing.Point(153, 55);
            this.label_parents_first_phone_number_value.Name = "label_parents_first_phone_number_value";
            this.label_parents_first_phone_number_value.Size = new System.Drawing.Size(201, 13);
            this.label_parents_first_phone_number_value.TabIndex = 23;
            this.label_parents_first_phone_number_value.Text = "label_parents_first_phone_number_value";
            // 
            // label_parents_first_email_value
            // 
            this.label_parents_first_email_value.AutoSize = true;
            this.label_parents_first_email_value.Location = new System.Drawing.Point(153, 68);
            this.label_parents_first_email_value.Name = "label_parents_first_email_value";
            this.label_parents_first_email_value.Size = new System.Drawing.Size(154, 13);
            this.label_parents_first_email_value.TabIndex = 24;
            this.label_parents_first_email_value.Text = "label_parents_first_email_value";
            // 
            // label_parents_second_email_value
            // 
            this.label_parents_second_email_value.AutoSize = true;
            this.label_parents_second_email_value.Location = new System.Drawing.Point(153, 148);
            this.label_parents_second_email_value.Name = "label_parents_second_email_value";
            this.label_parents_second_email_value.Size = new System.Drawing.Size(173, 13);
            this.label_parents_second_email_value.TabIndex = 34;
            this.label_parents_second_email_value.Text = "label_parents_second_email_value";
            // 
            // label_parents_second_phone_number_value
            // 
            this.label_parents_second_phone_number_value.AutoSize = true;
            this.label_parents_second_phone_number_value.Location = new System.Drawing.Point(153, 135);
            this.label_parents_second_phone_number_value.Name = "label_parents_second_phone_number_value";
            this.label_parents_second_phone_number_value.Size = new System.Drawing.Size(220, 13);
            this.label_parents_second_phone_number_value.TabIndex = 33;
            this.label_parents_second_phone_number_value.Text = "label_parents_second_phone_number_value";
            // 
            // label_parents_second_email
            // 
            this.label_parents_second_email.AutoSize = true;
            this.label_parents_second_email.Location = new System.Drawing.Point(6, 148);
            this.label_parents_second_email.Name = "label_parents_second_email";
            this.label_parents_second_email.Size = new System.Drawing.Size(42, 13);
            this.label_parents_second_email.TabIndex = 32;
            this.label_parents_second_email.Text = "E-Mail :";
            // 
            // label_parents_second_phone_number
            // 
            this.label_parents_second_phone_number.AutoSize = true;
            this.label_parents_second_phone_number.Location = new System.Drawing.Point(6, 135);
            this.label_parents_second_phone_number.Name = "label_parents_second_phone_number";
            this.label_parents_second_phone_number.Size = new System.Drawing.Size(82, 13);
            this.label_parents_second_phone_number.TabIndex = 31;
            this.label_parents_second_phone_number.Text = "Phone number :";
            // 
            // label_parents_second_patronym_value
            // 
            this.label_parents_second_patronym_value.AutoSize = true;
            this.label_parents_second_patronym_value.Location = new System.Drawing.Point(153, 122);
            this.label_parents_second_patronym_value.Name = "label_parents_second_patronym_value";
            this.label_parents_second_patronym_value.Size = new System.Drawing.Size(192, 13);
            this.label_parents_second_patronym_value.TabIndex = 30;
            this.label_parents_second_patronym_value.Text = "label_parents_second_patronym_value";
            // 
            // label_parents_second_name_value
            // 
            this.label_parents_second_name_value.AutoSize = true;
            this.label_parents_second_name_value.Location = new System.Drawing.Point(153, 109);
            this.label_parents_second_name_value.Name = "label_parents_second_name_value";
            this.label_parents_second_name_value.Size = new System.Drawing.Size(175, 13);
            this.label_parents_second_name_value.TabIndex = 29;
            this.label_parents_second_name_value.Text = "label_parents_second_name_value";
            // 
            // label_parents_second_surname_value
            // 
            this.label_parents_second_surname_value.AutoSize = true;
            this.label_parents_second_surname_value.Location = new System.Drawing.Point(153, 96);
            this.label_parents_second_surname_value.Name = "label_parents_second_surname_value";
            this.label_parents_second_surname_value.Size = new System.Drawing.Size(189, 13);
            this.label_parents_second_surname_value.TabIndex = 28;
            this.label_parents_second_surname_value.Text = "label_parents_second_surname_value";
            // 
            // label_parents_second_patronym
            // 
            this.label_parents_second_patronym.AutoSize = true;
            this.label_parents_second_patronym.Location = new System.Drawing.Point(6, 122);
            this.label_parents_second_patronym.Name = "label_parents_second_patronym";
            this.label_parents_second_patronym.Size = new System.Drawing.Size(57, 13);
            this.label_parents_second_patronym.TabIndex = 27;
            this.label_parents_second_patronym.Text = "Patronym :";
            // 
            // label_parents_second_name
            // 
            this.label_parents_second_name.AutoSize = true;
            this.label_parents_second_name.Location = new System.Drawing.Point(6, 109);
            this.label_parents_second_name.Name = "label_parents_second_name";
            this.label_parents_second_name.Size = new System.Drawing.Size(41, 13);
            this.label_parents_second_name.TabIndex = 26;
            this.label_parents_second_name.Text = "Name :";
            // 
            // label_parents_second_surname
            // 
            this.label_parents_second_surname.AutoSize = true;
            this.label_parents_second_surname.Location = new System.Drawing.Point(6, 96);
            this.label_parents_second_surname.Name = "label_parents_second_surname";
            this.label_parents_second_surname.Size = new System.Drawing.Size(55, 13);
            this.label_parents_second_surname.TabIndex = 25;
            this.label_parents_second_surname.Text = "Surname :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_students_surname);
            this.groupBox1.Controls.Add(this.label_students_surname_value);
            this.groupBox1.Controls.Add(this.label_students_name);
            this.groupBox1.Controls.Add(this.label_students_patronym);
            this.groupBox1.Controls.Add(this.label_students_name_value);
            this.groupBox1.Controls.Add(this.label_students_patronym_value);
            this.groupBox1.Controls.Add(this.label_students_phone_number);
            this.groupBox1.Controls.Add(this.label_students_phone_number_value);
            this.groupBox1.Controls.Add(this.label_students_email);
            this.groupBox1.Controls.Add(this.label_students_email_value);
            this.groupBox1.Controls.Add(this.label_students_group);
            this.groupBox1.Controls.Add(this.label_students_group_value);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(362, 117);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label_parents_first_surname);
            this.groupBox2.Controls.Add(this.label_parents_second_email_value);
            this.groupBox2.Controls.Add(this.label_parents_first_name);
            this.groupBox2.Controls.Add(this.label_parents_second_phone_number_value);
            this.groupBox2.Controls.Add(this.label_parents_first_patronym);
            this.groupBox2.Controls.Add(this.label_parents_second_email);
            this.groupBox2.Controls.Add(this.label_parents_first_surname_value);
            this.groupBox2.Controls.Add(this.label_parents_second_phone_number);
            this.groupBox2.Controls.Add(this.label_parents_first_name_value);
            this.groupBox2.Controls.Add(this.label_parents_second_patronym_value);
            this.groupBox2.Controls.Add(this.label_parents_first_patronym_value);
            this.groupBox2.Controls.Add(this.label_parents_second_name_value);
            this.groupBox2.Controls.Add(this.label_parents_first_phone_number);
            this.groupBox2.Controls.Add(this.label_parents_second_surname_value);
            this.groupBox2.Controls.Add(this.label_parents_first_email);
            this.groupBox2.Controls.Add(this.label_parents_second_patronym);
            this.groupBox2.Controls.Add(this.label_parents_first_phone_number_value);
            this.groupBox2.Controls.Add(this.label_parents_second_name);
            this.groupBox2.Controls.Add(this.label_parents_first_email_value);
            this.groupBox2.Controls.Add(this.label_parents_second_surname);
            this.groupBox2.Location = new System.Drawing.Point(12, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(362, 165);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parents";
            // 
            // button_edit
            // 
            this.button_edit.Location = new System.Drawing.Point(14, 319);
            this.button_edit.Name = "button_edit";
            this.button_edit.Size = new System.Drawing.Size(139, 23);
            this.button_edit.TabIndex = 37;
            this.button_edit.Text = "Edit";
            this.button_edit.UseVisualStyleBackColor = true;
            this.button_edit.Click += new System.EventHandler(this.button_edit_Click);
            // 
            // Form_ShowStudentsData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 353);
            this.Controls.Add(this.button_edit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label_studentsID);
            this.Controls.Add(this.label_studentsID_value);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_ShowStudentsData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form_ShowStudentsData";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_studentsID_value;
        private System.Windows.Forms.Label label_studentsID;
        private System.Windows.Forms.Label label_students_surname;
        private System.Windows.Forms.Label label_students_surname_value;
        private System.Windows.Forms.Label label_students_name;
        private System.Windows.Forms.Label label_students_patronym;
        private System.Windows.Forms.Label label_students_name_value;
        private System.Windows.Forms.Label label_students_patronym_value;
        private System.Windows.Forms.Label label_students_phone_number;
        private System.Windows.Forms.Label label_students_phone_number_value;
        private System.Windows.Forms.Label label_students_email;
        private System.Windows.Forms.Label label_students_email_value;
        private System.Windows.Forms.Label label_students_group;
        private System.Windows.Forms.Label label_students_group_value;
        private System.Windows.Forms.Label label_parents_first_surname;
        private System.Windows.Forms.Label label_parents_first_name;
        private System.Windows.Forms.Label label_parents_first_patronym;
        private System.Windows.Forms.Label label_parents_first_surname_value;
        private System.Windows.Forms.Label label_parents_first_name_value;
        private System.Windows.Forms.Label label_parents_first_patronym_value;
        private System.Windows.Forms.Label label_parents_first_phone_number;
        private System.Windows.Forms.Label label_parents_first_email;
        private System.Windows.Forms.Label label_parents_first_phone_number_value;
        private System.Windows.Forms.Label label_parents_first_email_value;
        private System.Windows.Forms.Label label_parents_second_email_value;
        private System.Windows.Forms.Label label_parents_second_phone_number_value;
        private System.Windows.Forms.Label label_parents_second_email;
        private System.Windows.Forms.Label label_parents_second_phone_number;
        private System.Windows.Forms.Label label_parents_second_patronym_value;
        private System.Windows.Forms.Label label_parents_second_name_value;
        private System.Windows.Forms.Label label_parents_second_surname_value;
        private System.Windows.Forms.Label label_parents_second_patronym;
        private System.Windows.Forms.Label label_parents_second_name;
        private System.Windows.Forms.Label label_parents_second_surname;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_edit;
    }
}