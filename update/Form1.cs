﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

using SevenZip;

namespace update
{
    public partial class Form1 : Form
    {
        string settingsXMLPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\EJournal\\settings.xml";

        string availableVersion;
        string filename;
        string link;

        public Form1()
        {
            InitializeComponent();

            Thread updateThread = new Thread(downloadUpdate)
            {
                IsBackground = true
            };
            updateThread.Start();
        }

        private void ExtractingProgressCallback(object sender, ProgressEventArgs e)
        {
            if (label1.InvokeRequired)
            {
                label1.Invoke(new Action(delegate () { label1.Text = "Install progress: " + e.PercentDone + "%"; }));
            }
            else
            {
                label1.Text = "Install progress: " + e.PercentDone + "%";
            }

            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(new Action(delegate () { progressBar1.Value = e.PercentDone; }));
            }
            else
            {
                progressBar1.Value = e.PercentDone;
            }
        }

        private void ExtractingFinishedCallback(object sender, EventArgs e)
        {
            Thread.Sleep(500);
            if (label1.InvokeRequired)
            {
                pictureBox1.Invoke(new Action(delegate () { pictureBox1.Image = Properties.Resources.ejournal_success; }));
                label1.Invoke(new Action(delegate () { label1.Text = "Installed"; }));
            }
            else
            {
                pictureBox1.Image = Properties.Resources.ejournal_success;
                label1.Text = "Installed";
            }

            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(new Action(delegate () { progressBar1.Value = 100; }));
            }
            else
            {
                progressBar1.Value = 100;
            }
        }

        private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
        {
            if (label1.InvokeRequired)
            {
                label1.Invoke(new Action(delegate () { label1.Text = "Download progress: " + e.ProgressPercentage + "%"; }));
            }
            else
            {
                label1.Text = "Download progress: " + e.ProgressPercentage + "%";
            }

            if (progressBar1.InvokeRequired)
            {
                progressBar1.Invoke(new Action(delegate () { progressBar1.Value = e.ProgressPercentage; }));
            }
            else
            {
                progressBar1.Value = e.ProgressPercentage;
            }

        }

        private void DownloadCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            Thread.Sleep(500);
            if (e.Error == null)
            {
                if (label1.InvokeRequired)
                {
                    label1.Invoke(new Action(delegate () { label1.Text = "Download completed!"; }));
                }
                else
                {
                    label1.Text = "Download completed!";
                }

                SevenZipBase.SetLibraryPath("x86\\7z.dll");
                SevenZipExtractor extractor = new SevenZipExtractor(filename);
                extractor.Extracting += new EventHandler<ProgressEventArgs>(ExtractingProgressCallback);
                extractor.ExtractionFinished += new EventHandler<EventArgs>(ExtractingFinishedCallback);
                extractor.ExtractArchive(System.IO.Path.GetDirectoryName(Application.ExecutablePath));

                File.Delete(filename);

                XDocument settings;
                try
                {
                    settings = XDocument.Load(settingsXMLPath);
                }
                catch
                {
                    settings = new XDocument(
                        new XElement("settings",
                            new XElement("version", ""),
                            new XElement("language", "Українська"),
                            new XElement("username", "Anonymous")
                        )
                    );
                }
                settings.Element("settings").Element("version").Value = availableVersion;
                settings.Save(settingsXMLPath);

                for (int i = 5; i > 2; i--)
                {
                    if (label1.InvokeRequired)
                    {
                        label1.Invoke(new Action(delegate () { label1.Text = "Installed (" + (i-2) + ")"; }));
                    }
                    else
                    {
                        label1.Text = "Installed (" + (i-2) + ")";
                    }
                    Thread.Sleep(1000);
                }

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "ejournal.exe";
                Process.Start(startInfo);
                Application.Exit();
            }
        }

        public void downloadUpdate()
        {
            try
            {
                if (label1.InvokeRequired)
                {
                    label1.Invoke(new Action(delegate () { label1.Text = "Connecting..."; }));
                }
                else
                {
                    label1.Text = "Connecting...";
                }
                
                if(progressBar1.InvokeRequired)
                {
                    progressBar1.Invoke(new Action(delegate () { progressBar1.Style = ProgressBarStyle.Marquee; }));
                }
                else
                {
                    progressBar1.Style = ProgressBarStyle.Marquee;
                }
                

                Ping ping = new Ping();
                PingReply p = ping.Send("ejournal.ga");

                if (p.Status.ToString().Equals("Success"))
                {
                    WebRequest requestUpdateData = WebRequest.Create("http://ejournal.ga/updateService/update.xml");
                    WebResponse responseUpdateData = requestUpdateData.GetResponse();
                    Stream answerStream = responseUpdateData.GetResponseStream();
                    StreamReader srAnswer = new StreamReader(answerStream);
                    string updateData = srAnswer.ReadToEnd();

                    XDocument updateXML = XDocument.Parse(updateData);
                    availableVersion = updateXML.Element("update").Element("version").Value;
                    filename = updateXML.Element("update").Element("link").Value;
                    link = "http://ejournal.ga/updateService/builds/" + filename;

                    if(label1.InvokeRequired)
                    {
                        label1.Invoke(new Action(delegate () { label1.Text = availableVersion; }));
                    }
                    else
                    {
                        label1.Text = availableVersion;
                    }

                    Thread.Sleep(500);

                    if (progressBar1.InvokeRequired)
                    {
                        progressBar1.Invoke(new Action(delegate () { progressBar1.Style = ProgressBarStyle.Continuous; }));
                    }
                    else
                    {
                        progressBar1.Style = ProgressBarStyle.Continuous;
                    }

                    WebClient webClient = new WebClient();
                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadCompletedCallback);
                    webClient.DownloadFileAsync(new System.Uri(link), filename);
                }

                else
                {
                    if(label1.InvokeRequired)
                    {
                        label1.Invoke(new Action(delegate () { label1.Text = "No connection"; }));
                        pictureBox1.Invoke(new Action(delegate () { pictureBox1.Image = Properties.Resources.ejournal_fail; }));
                    }
                    else
                    {
                        label1.Text = "No connection";
                        pictureBox1.Image = Properties.Resources.ejournal_fail;
                    }

                    if (progressBar1.InvokeRequired)
                    {
                        progressBar1.Invoke(new Action(delegate () { progressBar1.Style = ProgressBarStyle.Continuous; }));
                    }
                    else
                    {
                        progressBar1.Style = ProgressBarStyle.Continuous;
                    }

                    if (this.InvokeRequired)
                    {
                        this.Invoke(new Action(delegate () { MessageBox.Show("No connection"); }));
                    }
                    else
                    {
                        MessageBox.Show("No connection");
                    }
                    Application.Exit();
                }
            }
            catch(PingException)
            {
                if (label1.InvokeRequired)
                {
                    label1.Invoke(new Action(delegate () { label1.Text = "No connection"; }));
                }
                else
                {
                    label1.Text = "No connection";
                }

                if (this.InvokeRequired)
                {
                    this.Invoke(new Action(delegate () { MessageBox.Show("No connection"); }));
                }
                else
                {
                    MessageBox.Show("No connection");
                }

                Application.Exit();
            }
        }
    }
}